<?php
	session_start();
	$Dir	= getcwd() . '/' . 'Assets/gtapev2/fonts/';  
	$String	= '';
	for($i=0;$i<7;$i++):
		$String .= chr(rand(97,122));
	endfor;
	$_SESSION['captcha'] = $String;
	$image = imagecreatetruecolor(170, 60);
	$black = imagecolorallocate($image, 0, 0, 0);
	$color = imagecolorallocate($image, 200, 100, 90); // red
	$white = imagecolorallocate($image, 244, 244, 244);
	imagefilledrectangle($image,0,0,200,100,$white);
	imagettftext($image, 30, 0, 10, 40, $color, $Dir."OpenSans-Light.ttf", $_SESSION['captcha']);
	header("Content-type: image/png");
	imagepng($image);
?>