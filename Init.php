<?php
/**
 * Khafii Semi-Framework
 * 
 * @name		Init.php
 * @author		Alfin B.N.I.A (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

define('_APPS_', 'Apps');
define('_LIB_', 'Library');
define('_LOAD_', 'Load');
putenv("NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1"); 
 
require_once __DIR__ . '/Library/Load.php';

session_start();
$Khafii->Output->Start();

if(!$Khafii->Handling->Set(SELF_PARENT)):
	$Khafii->InstantControll->Redirect($Khafii->Default);
else:
	require_once __DIR__ . '/Apps/' . SELF_PARENT . '/' . 'Index.php';
endif; 	