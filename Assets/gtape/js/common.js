$(document).ready(function(e){
	setInterval(ajaxClock, 1000);
	$(this).bind("contextmenu", function(e){
		e.preventDefault();
	});
});
$('a.class-bar').click(function(e){
	$(this).addClass('active');
});
$(function(){
	$('#datepicker').datepicker().on('changeDate', function(ev){
		$('#datepicker').datepicker('hide');
	});
});
//=====================================> Menampilkan hasil pencarian elektor didalam modal
$('#formElector').on('submit',function(ev){
	var kode	= $('.nim').val();
	var name	= $('.name').val();
	var dob		= $('.dob').val();
	var father	= $('.father').val();
	if(kode == ''){
		$("#dialogInfoElector").modal('show');
		$('span#pg').text('asdasdasd');
		$('#dataElector').load('gtape!elector.information.json',{"kode":kode,"nama":name,"dob":dob,"father":father},
			function(responseText, textStatus, XMLHttpRequest){
				if(textStatus	= "success"){
					$('#bodyElector').show();
					$('img#load').hide();
				}
			}
		);
		$('#bodyElector').hide();
		$('img#load').show();
		return false;
	} else {
		$("#dialogInfoElector").modal('hide');
	}
});
//====================================> Menampilkan hasil melalui $.ajax(post)
$(".openResult").click(function(event){
	$('#postLoad').css("display","inline");
	var datePost	= $('.datePost').val();
	var agePost 	= $('.agePost').val();
	var paramPost 	= $('.paramPost').val();
	$.post(
		"gtape!chart.table.json",
		{date:datePost,age:agePost,param:paramPost},
		function(status){
			$.getJSON("data.json", function(data,status){
                
                if(status	= "success"){
					$('#postLoad').css("display","none");
				} else {
					$('#postLoad').css("display","block");
				}
				
				//=========================> Inisialisasi untuk variabel data berdasarkan jenis kelamin pemilih
                var TotalMan        = 0;
                var TotalWoman      = 0;
                var TotalElector    = 0;
                var TotalPercentMan = 0;
                var TotalPercentWoman   = 0;
                var TotalPercentElector = 0;
                //=========================> Inisialisasi untuk variabel data berdasarkan umur pemilih
				var TotalYoung		= 0;
				var TotalAdult		= 0;
				var TotalGrown		= 0;
				var TotalOld		= 0;
				
                for(var i = 1; i < 11; i++){
					//===============================> Inisialisasi Kode Region
                    var p      = i < 10 ? '0' + i : i;
                    var q      = 9+i
                    //===============================================> Data berdasarkan jenis kelamin pemilih
                    var Man    = parseInt(data[i-1]['TOTAL_PRIA']);
                    var Woman  = parseInt(data[i-1]['TOTAL_WANITA']);
                    var Electr = parseInt(data[i-1]['TOTAL_ELEKTOR']);
                    var All    = parseInt(data[i-1]['TOTAL_ALL']);  
                    
                    TotalMan       += parseInt(Man);
					TotalWoman     += parseInt(Woman);
                    TotalElector   += parseInt(Electr);
                    
                    PercentMan     = parseInt(Man) / parseInt(All) * 100;
                    PercentWoman   = parseInt(Woman) / parseInt(All) * 100;
                    PercentElector = parseInt(Electr) / parseInt(All) * 100;
                    
                    TotalPercentMan     += PercentMan;
                    TotalPercentWoman   += PercentWoman;
                    TotalPercentElector += PercentElector;
                    
                    //==================================================> Menampilkan hasil
                    $('td#mt' + p).text(ReplaceNumberWithCommas(Man));
					$('td#wt' + p).text(ReplaceNumberWithCommas(Woman));
					$('td#tt' + p).text(ReplaceNumberWithCommas(Electr));
					//===========================================> Menampilkan hasil dalam bentuk Persentase
                    $('td#mp' + p).text(PercentMan.toFixed(2));
					$('td#wp' + p).text(PercentWoman.toFixed(2));
					$('td#tp' + p).text(PercentElector.toFixed(2));
                    //==========================================> Data berdasarkan usia pemilih
					var Young 	= parseInt(data[q]['UMUR_16']) + parseInt(data[q]['UMUR_17']);
					var Adult	= parseInt(data[q]['UMUR_21']) + parseInt(data[q]['UMUR_26']) + parseInt(data[q]['UMUR_31']) + parseInt(data[q]['UMUR_36']) + parseInt(data[q]['UMUR_41']);
					var Grown	= parseInt(data[q]['UMUR_46']) + parseInt(data[q]['UMUR_51']) + parseInt(data[q]['UMUR_56']);
					var Old		= parseInt(data[q]['UMUR_61']);
                    
                    TotalAdult     += parseInt(Adult);
                    TotalGrown     += parseInt(Grown);
                    TotalOld       += parseInt(Old);
                    TotalYoung     += parseInt(Young);
                    //=================================================> Menampilkan hasil
                    $('td#ay' + p).text(ReplaceNumberWithCommas(Young));
                    $('td#aa' + p).text(ReplaceNumberWithCommas(Adult));
                    $('td#ag' + p).text(ReplaceNumberWithCommas(Grown));
                    $('td#ao' + p).text(ReplaceNumberWithCommas(Old));
                    $('td#at' + p).text(ReplaceNumberWithCommas(
						parseInt(Young) + parseInt(Adult) + parseInt(Grown) + parseInt(Old) 
					));
                    
                };
                //========================================> Menampilkan hasil keseluruhan data berdasarkan jenis kelamin pemilih
				$('td#mtt').css("font-weight","bold").text(
                    ReplaceNumberWithCommas(TotalMan)
				);
                $('td#mtp').css("font-weight","bold").text(
                    ReplaceNumberWithCommas(TotalPercentMan.toFixed(2))
				);
				$('td#wtt').css("font-weight","bold").text(
                    ReplaceNumberWithCommas(TotalWoman)
				);
                $('td#wtp').css("font-weight","bold").text(
                    ReplaceNumberWithCommas(TotalPercentWoman.toFixed(2))
				);
				$('td#ttt').css("font-weight","bold").text(
					ReplaceNumberWithCommas(TotalElector)
				);
                $('td#ttp').css("font-weight","bold").text(
                    ReplaceNumberWithCommas(TotalPercentElector.toFixed(2))
				);
				//========================================> Menampilkan hasil keseluruhan data berdasarkan umur pemilih
				$('td#tay').css("font-weight","bold").text(
                    ReplaceNumberWithCommas(TotalYoung)
				);
                $('td#taa').css("font-weight","bold").text(
                    ReplaceNumberWithCommas(TotalAdult)
				);
                $('td#tag').css("font-weight","bold").text(
                    ReplaceNumberWithCommas(TotalGrown)
				);
                $('td#tao').css("font-weight","bold").text(
                    ReplaceNumberWithCommas(TotalOld)
				);
                $('td#tat').css("font-weight","bold").text(
                    ReplaceNumberWithCommas(
						parseInt(TotalYoung) + parseInt(TotalAdult) + parseInt(TotalGrown) + parseInt(TotalOld)
					)
				);
			});
		}
	);
});
$("input:checkbox").click(function() {
    if ($(this).is(":checked")) {
        var group = "input:checkbox[name='" + $(this).attr("name") + "']";
        $(group).prop("checked", false);
        $(this).prop("checked", true);
    } else {
        $(this).prop("checked", false);
    }
});
function ReplaceNumberWithCommas(yourNumber) {
    var n= yourNumber.toString().split(".");
    n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    return n.join(".");
}

document.onkeydown = function(){
	switch (event.keyCode){
	}
}
function disableCopyPaste(e){
	return false
}
function enableCopyPaste(){
	return true
}
document.onselectstart = new Function ("return false")
if (window.sidebar){
	document.onmousedown	= disableCopyPaste
	document.onclick		= enableCopyPaste
}
function InputAngka(InputKarakter){
	var karakter = (InputKarakter.which) ? InputKarakter.which : event.keyCode
	return !(karakter > 31 && (karakter < 48 || karakter > 57));
}
function ajaxClock(){
	$.ajax({
		url : 'gtapedate',
		success : function(data){
			$('#jam').html(data);	
		},
	});
}	
var x;
setInterval(function() {
	if(x == 0) {
		$('.blink').css('color', 'black');
		x = 1;
	} else {
		if(x = 1) {
			$('.blink').css('color', 'red'); 
			x = 0;
		}
	} 			
}, 500);