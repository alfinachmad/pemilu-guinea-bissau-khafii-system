<?php
	session_start();
	date_default_timezone_set('Africa/Bissau');
	$Array	= array(
		'English' => array(
			'Day'	=> array('','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'),
			'Month'	=> array('','January','February','March','April','May','June','July','August','September','October','November','December')
		),
		'Bahasa' => array(
			'Day'	=> array('','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu'),
			'Month'	=> array('','Januari','Februar','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember')
		),
		'Portuguese' => array(
			'Day'	=> array('','Segunda-feira','Terca-feira','Quarta-feira','Quinta-feira','Sexta-feira','S�bado','Domingo'),
			'Month'	=> array('','Janeiro','Fevereiro','Mar�o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro')
		),
		'Tetun' => array(
			'Day'	=> array('','Segunda','Tersa','Quarta','Quinta','Sexta','S�bado','Domingo'),
			'Month'	=> array('','Janeiro','Fevereiro','Marsu','Abril','Maiu','Junhu','Julhu','Agostu','Setembru','Outubru','Novembru','Dezembru')
		),
	);
	$Day	= $Array[$_SESSION['langToken']]['Day'];
	$Month	= $Array[$_SESSION['langToken']]['Month'];
	if(@$_GET['id'] == 'short'):
		echo date('D, d M Y - H:i:s');
	else:
		echo str_replace(date('N'),$Day[date('N')],date('N')) . date(', d ') . str_replace(date('n'),$Month[date('n')],date('n')) . date(' Y - H:i:s');
	endif;
?>