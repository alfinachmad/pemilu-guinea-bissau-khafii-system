<?php
	$Lang	= $this->Library->InstantLanguage->Load();
	$Lang	= $Lang['Table'];
?>
<section class="panel">
	<header class="panel-heading">
    	<?=$Lang['Left_Text_Content']?> <label class="label bg-danger m-l-xs">Bar Chart</label>
        <span class="pull-right text-sm"><?=next($Lang)?> : 
        	<span class="dfn">
				<?=$this->Library->InstantInfo->GetLastUpdate(basename(__DIR__),'d F Y - H:i:s');?>
            </span>
        </span>
    </header>
    <div class="panel-body"> 
    	<div class="tab-content"> 
        	<?php
			$Parse = $this->Library->Region->ListRegion();
			$this->Library->Database->Execute(
				array($Parse)
			);
			$i		= 1;
			$Alias	= '';
			$Region	= '';
			while($Result = $this->Library->Database->FetchAssoc($Parse)):
			if($Result['NAMA'] == 'AFRICA - EUROPA'):
				$Alias	= 'AFR/EUR';
				$Region = 'DIASPORA';
			elseif($Result['NAMA'] == 'BOLAMA - BIJAGOS'):
				$Region	= 'BOLAMA';
			elseif($Result['NAMA'] == 'SAB'):
				$Alias	= 'SAB';
				$Region	= 'BISSAU';
			else:
				$Region	= $Result['NAMA'];
			endif;
        	?>
			<div class="tab-pane<?=$i == 1? ' active in' : ''?> fade" id="<?=$this->Library->String->Charset($Region.$Result['KODE_REGION'])?>">
            	<div class="panel-body text-center">
                	<div id="CHART<?=$this->Library->String->Charset($Region)?>" class="content-chart">
                    
                    </div>
                </div>
            </div> 
            <?php $i++; endwhile; ?>
			<div class="tab-pane fade" id="ALL">
            	<div class="panel-body text-center">
                	<div id="CHARTNASIONAL" class="content-chart">
                    
                    </div>
                </div>
            </div> 
		</div>
 	</div>   
	<footer class="panel-heading bg-danger"> 
    	<ul class="nav nav-tabs nav-justified">
        	<?php
			$Lang	= $this->Library->InstantLanguage->Load();
			$this->Library->Database->Execute(
				array($Parse)
			);
			$i		= 1;
			$Region	= '';
			while($Result = $this->Library->Database->FetchAssoc($Parse)):
			if($Result['NAMA'] == 'AFRICA - EUROPA'):
				$Alias	= 'AFR/EUR';
				$Region = 'DIASPORA';
			elseif($Result['NAMA'] == 'BOLAMA - BIJAGOS'):
				$Region	= 'BOLAMA';
				$Alias	= $Region;
			elseif($Result['NAMA'] == 'SAB'):
				$Region	= 'BISSAU';
				$Alias	= $Region;
			else:
				$Region	= $Result['NAMA'];
				$Alias	= $Region;
			endif;
			?>
        	<li <?=$i == 1? 'class="active"' : ''?>>
            	<a href="#<?=$this->Library->String->Charset($Region.$Result['KODE_REGION'])?>" data-toggle="tab"><?=$this->Library->String->Charset($Alias)?></a>
            </li> 
            <?php $i++; endwhile; ?>
        	<li>
            	<a href="#ALL" data-toggle="tab"><?=$Lang['ContentBar']['Other_Text1']?></a>
            </li>
        </ul>
	</footer>
</section>