<?php
	$Sess	= $_SESSION[SELF_PARENT];
	$SExp	= $Khafii->String->Exploding($Khafii->Security->Decrypt($Sess),'||||');
	$Link	= $Khafii->Controll->Link('LINK-PARENT');
	if(isset($_GET['ltoken'])):
		$_SESSION['langToken']	= $Khafii->Config['HTML']['Language'];
		if($_GET['ltoken'] == '44'):
			$_SESSION['langToken'] = 'English';		
			$Khafii->InstantControll->Redirect($Khafii->Default);
		elseif($_GET['ltoken'] == '351'):
			$_SESSION['langToken'] = 'Portuguese';
			$Khafii->InstantControll->Redirect($Khafii->Default);
		endif;
	endif;
	$Lang	= $Khafii->InstantLanguage->Load();
	$Lang	= $Lang['Front'];
	define('SESSIONACTIVE',$SExp[0]);
	define('IDPROFILE',$SExp[1]);
?>
<!DOCTYPE html>
	<html lang="en"><head>
		<title><?=$Khafii->Config['App']['Description']?></title>
        <meta charset="ISO-8859-1"> 
		<meta name="description" content="<?=$Khafii->Config['App']['Description']?>">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="<?=Stylsheet?>basic">
		<link rel="stylesheet" href="<?=Font?>fonts">
        <style type="text/css">
			<?php if($SExp[0] != 'Administrator'): ?>
			@media print{
				* {display: none !important;}
			}
			<?php endif; ?>
        	#contentbody {
				background: url("<?=Image?>avataropc") #FFF no-repeat center 53% fixed; 
				-webkit-background-size: 25%;
				-moz-background-size: 25%;
				-o-background-size: 25%;
				background-size: 30%;
			}
        </style>
	</head>
	<body>
	<?php
		$Exp	= $Khafii->String->Exploding(SELF_SUB_PARENT,'.');
    	if(!$Khafii->Handling->Set($_SESSION[SELF_PARENT])):
			$Khafii->Controll->Routing('Authentication');
			$Khafii->FileSystem->FilePut($Khafii->Config['json']['src'],'NULL');
		elseif($Khafii->Handling->Set(SELF_SUB_PARENT) && (SELF_SUB_PARENT == 'elector.information.json' || SELF_SUB_PARENT == 'chart.table.json')):
			$Direct = $Khafii->String->UpperWords($Khafii->String->Replace('.',' ',$Exp[0].'.'.$Exp[1],'R'),'R');
			$Folder	= $Khafii->String->UpperWords($Khafii->String->Replace('.',' ',$Exp[0].'.'.$Exp[1].'.'.$Exp[2],'R'),'R');
			$Direct = $Khafii->String->Replace(' ','.',$Direct,'R');
			$Folder	= $Khafii->String->Replace(' ','.',$Folder,'R');
			$Khafii->Controll->Routing(
				$Direct,$Folder
			);
		else:
			if(SELF_SUB_PARENT == ERROR404):
				$Khafii->Controll->Routing('Error.Index');
			else:
    ?>
		<section class="hbox stretch">
			<aside class="bg-primary aside-sm nav-vertical" id="nav">
				<section class="vbox">
					<header class="dker nav-bar" style="background-color:#00853D !important;"> 
                		<a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="body"> 
                    		<i class="icon-reorder"></i> 
                    	</a> 
                    	<a href="<?=$Khafii->Controll->Link('PARENT')?>" class="nav-brand">GTAPE</a> 
                    	<a class="btn btn-link visible-xs" data-toggle="class:show" data-target=".nav-user"> 
                    		<i class="icon-comment-alt"></i> 
                    	</a> 
                	</header> 
                	<footer class="footer bg-gradient hidden-xs"> 
                		<a href="<?=$Link?>logout" class="btn btn-sm btn-link m-r-n-xs pull-right"> 
                    		<i class="icon-off"></i> 
                    	</a> 
                    	<a data-toggle="class:nav-vertical" class="btn btn-sm btn-link m-l-n-sm"> 
                    		<i class="icon-reorder"></i> 
                    	</a> 
                	</footer>
                	<section>
                		<div class="nav-user hidden-xs pos-rlt">
                			<div class="nav-avatar pos-rlt">
                				<a class="thumb-sm avatar animated <?=!$Khafii->Handling->Set(SELF_PARENT)?'rollIn':''?>" data-toggle="dropdown">
                					<img src="<?=Image?>avatar" alt="" class=""> 
                				</a>
                				<div class="visible-xs m-t m-b"> 
	                             	<a class="h4"></a> 
	                                <p>
	                                	<i class="icon-map-marker"></i> Bissau, Guinea-Bissau
	                                </p> 
                             	</div>   				
                			</div>
                			<div class="nav-msg">
                				<a class="dropdown-toggle" data-toggle="dropdown">
                					<b class="badge badge-white count-n">0</b>
                				</a>
                			</div>
                		</div>
                		<nav class="nav-primary hidden-xs">
                			<ul class="nav">
                                <li<?=SELF_SUB_PARENT=='elector.information'?' class="active"':''?> style="border-top:1px solid rgba(0,0,0,0.075) !important;">
                                	<a id="push" class="class-bar" href="<?=$Link?>elector.information"> 
                                    	<i class="icon-list-alt" style="color: #FFF !important;"></i> 
                                        <span style="color: #FFF !important;">Elector Information</span>
                                   	</a>
                                </li> 
                                <li<?=SELF_SUB_PARENT=='chart.table'?' class="active"':''?> style="border-top:1px solid rgba(0,0,0,0.075) !important;">
                                	<a id="push" class="class-bar" href="<?=$Link?>chart.table"> 
                                    	<i class="icon-tasks" style="color: #FFF !important;"></i> 
                                        <span style="color: #FFF !important;">Table Chart</span>
                                   	</a>
                                </li> 
                                <li <?=SELF_SUB_PARENT=='chart.bar'?'class="active"':''?>> 
                                	<a id="push" class="class-bar" href="<?=$Link?>chart.bar"> 
                                    	<i class="icon-bar-chart" style="color: #FFF !important;"></i> 
                                        <span style="color: #FFF !important;">Bar Chart</span> 
                                    </a>
                                </li>
                				<li <?=SELF_SUB_PARENT=='chart.pie'?'class="active"':''?>> 
                                	<a id="push" class="class-pie" href="<?=$Link?>chart.pie"> 
                                    	<i class="icon-certificate" style="color: #FFF !important;"></i> 
                                        <span style="color: #FFF !important;">Pie Chart</span> 
                                    </a> 
                                </li> 
                				<li <?=SELF_SUB_PARENT=='chart.line'?'class="active"':''?>> 
                                	<a id="push" class="class-line" href="<?=$Link?>chart.line"> 
                                    	<i class="icon-tasks" style="color: #FFF !important;"></i> 
                                        <span style="color: #FFF !important;">Line Chart</span> 
                                    </a> 
                                </li> 
                			</ul>
                		</nav>
                	</section>
				</section>
			</aside>
			<section id="content">
            	<?php if($Khafii->Config['DB']['Status'] != 'Disable' || $Khafii->Database->Connect()): else: ?>
                <div class="bg-danger wrapper hidden-vertical animated rollIn text-sm"> 
                    <a href="#" data-dismiss="alert" class="pull-right m-r-n-sm m-t-n-sm"><i class="icon-close icon-remove "></i></a> 
                        <?=$Lang['First_Text1']?>
                </div>			
                <?php endif; ?>	
                <section class="vbox">
					<header class="header bg-white b-b" align="center">
						<p id="jam" style="float: left !important;"></p>
                        <p style="float: right !important;">
							<?=$Lang['First_Text']?> <strong><?=$_SESSION['modeprofile'] == 'Active' ? $SExp[1] : $SExp[0]?></strong>
                            <?php if($Khafii->Config['DB']['Status'] == 'SafeMode'): ?>
                            <sup> <strong style="color:#F00;" class="blink">Safe Mode</strong></sup>
                            <?php endif; ?>
                        </p>
					</header>
					<section class="scrollable wrapper">
						<div class="row">
							<div id="content-load" class="col-lg-12">
                            	<?php 
									if($Khafii->Handling->Set(SELF_SUB_PARENT)):
										if(SELF_SUB_PARENT == 'logout'):
											$Khafii->Session->SessionUnset($Sess);
											$Khafii->Session->SessionDestroy();
											$Khafii->FileSystem->FilePut($Khafii->Config['json']['src'],'NULL');
											if($_SESSION['langToken'] == 'Portuguese'):
												$Khafii->InstantControll->Redirect(SELF_PARENT . '_t351');
											else:
												$Khafii->InstantControll->Redirect(SELF_PARENT);
											endif;
										else:
											if(count($Exp) > 2): 
												$Direct = $Khafii->String->UpperWords($Khafii->String->Replace('.',' ',$Exp[0].'.'.$Exp[1],'R'),'R');
												$Folder	= $Khafii->String->UpperWords($Khafii->String->Replace('.',' ',$Exp[0].'.'.$Exp[1].'.'.$Exp[2],'R'),'R');
												$Direct = $Khafii->String->Replace(' ','.',$Direct,'R');
												$Folder	= $Khafii->String->Replace(' ','.',$Folder,'R');
												$Khafii->Controll->Routing(
													$Direct,$Folder
												);
											else:
												$Direct = $Khafii->String->UpperWords($Khafii->String->Replace('.',' ',SELF_SUB_PARENT,'R'),'R');
												$Direct = $Khafii->String->Replace(' ','.',$Direct,'R');
												$Khafii->Controll->Routing(
													$Direct
												);
											endif;
										endif;
									else:
										$Khafii->Controll->Routing('Default');
									endif;
								?>
                            </div>
						</div>
					</section>
				</section>
				<a class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="body"></a>
			</section>
		</section>
        <?php endif; endif; ?>
		<script src="<?=Javascript?>jquery"></script>
		<script src="<?=Javascript?>basic"></script>
		<script src="<?=Javascript?>common"></script>
        <?php 
			if(SELF_SUB_PARENT == 'chart.bar' || SELF_SUB_PARENT == 'chart.pie' || SELF_SUB_PARENT == 'chart.line'): 
		?>
		<script src="<?=Javascript?>highcharts"></script>
		<script src="<?=Javascript?>export"></script>
        <?php endif; ?>
        <script>
			<?php 
				if(SELF_SUB_PARENT == 'chart.bar'): 
				$Lang	= $Khafii->InstantLanguage->Load();
				$Lang	= $Lang['ContentBar'];
				$Cursor = $Khafii->Elector->ByRegion('ALL');  
				while($Data	= $Khafii->Database->FetchAssoc($Cursor)):
				if($Data['NAMA_REGION'] == 'AFRICA - EUROPA'):
					$Alias	= 'AFR/EUR';
					$Region = 'DIASPORA';
				elseif($Data['NAMA_REGION'] == 'BOLAMA - BIJAGOS'):
					$Region	= 'BOLAMA';
				elseif($Data['NAMA_REGION'] == 'SAB'):
					$Alias	= 'SAB';
					$Region	= 'BISSAU';
				else:
					$Region	= $Data['NAMA_REGION'];
				endif;
			?>
			
			$(function(){
				var colors = Highcharts.getOptions().colors,
					categories = ['<?=next($Lang)?>', '<?=next($Lang)?>','Total'],
					name = 'Last updated data in <?=$Khafii->DTF->DTFFormat()?>',
					data = [{y: <?=$Data['TOTAL_PRIA']?>,color: colors[0]},{y: <?=$Data['TOTAL_WANITA']?>,color: colors[1]},{y: <?=$Data['TOTAL_WANITA']+$Data['TOTAL_PRIA']?>,color: colors[2]}];
				var chart = $('#CHART<?=$Khafii->String->Charset($Region)?>').highcharts({
					chart:{type:'column'},title:{text:'<?=reset($Lang)?> : <b><?=$Khafii->String->Charset($Region)?></b>'},
					xAxis:{categories:categories},yAxis:{title:{text:''},labels:{formatter:function(){return Highcharts.numberFormat(this.value,0)}}},
					legend:{enabled:false,layout:'vertical',backgroundColor:'#FFFFFF',align:'center',verticalAlign:'bottom',floating:true,shadow:true},
					plotOptions:{column:{cursor:'pointer',dataLabels:{enabled:true,color:colors[0],style:{fontWeight:'bold'},
						formatter:function(){return Highcharts.numberFormat(this.y, 0, '.')}},
						events:{legendItemClick:function(){return false;}}},allowPointSelect:false,
					},
					tooltip:{
						formatter:function(){var point=this.point,s=this.x +':<b> '+Highcharts.numberFormat(this.y, 0, '.') +' Elector</b><br/>';
							return s;
						}
					},series:[{name:name,data:data,color:'#009E49'}],
					exporting: {buttons:{contextButton:{menuItems:[{text:'Print Chart',onclick:function(){this.print()}},
						{text:'Export to PNG',onclick:function(){
							var x = prompt('Input file name','CHART_BAR_<?=$Region?>_<?=$Khafii->DTF->DTFFormat('dmY_His')?>');
							if(x != null){this.exportChart({filename: x})} 
							else{x = 'CHART_BAR_<?=$Region?>_<?=$Khafii->DTF->DTFFormat('dmY_His')?>'; this.exportChart({filename: x})}
						}}, 
						{text:'Export to PDF',onclick: function(){
							var x = prompt('Input file name','CHART_BAR_<?=$Khafii->String->Charset($Region)?>_<?=$Khafii->DTF->DTFFormat('dmY_His')?>');
							if(x != null){this.exportChart({filename: x,type: "application/pdf"})} 
							else{x = 'CHART_BAR_<?=$Region?>_<?=$Khafii->DTF->DTFFormat('dmY_His')?>'; this.exportChart({filename: x,type: "application/pdf"});
						}},}]}},enabled: <?= $SExp[0] != 'Administrator' ? 'false' : 'true' ?>,sourceWidth: 800,sourceHeight: 600}
				})
			})
			
			<?php endwhile; $Parse	= $Khafii->Region->ListRegion('ALL') ?>
			
			$(function () {
				Highcharts.setOptions({colors: ['#CE1126','#009E49','#DDDF00','#24CBE5','#64E572','#FF9655','#FFF263','#6AF9C4']});
				$('#CHARTNASIONAL').highcharts({
					chart: {type: 'column'},title: {text: '<b><?=$Lang['Other_Text2']?></b>'},
					xAxis: {
						categories: [
						<?php 
						while($Result	= $Khafii->Database->FetchAssoc($Parse)): 
						if($Result['NAMA'] == 'AFRICA - EUROPA'):
							$Alias	= 'AFR/EUR';
							$Region = $Alias;
						elseif($Result['NAMA'] == 'BOLAMA - BIJAGOS'):
							$Region	= 'BOLAMA';
						elseif($Result['NAMA'] == 'SAB'):
							$Alias	= 'BISSAU';
							$Region	= $Alias;
						else:
							$Alias	= $Result['NAMA'];
							$Region	= $Alias;
						endif;
						echo "'".$Khafii->String->Charset($Alias)."',";
						endwhile; 
						echo "'NATIONAL'";
						$Parse	= $Khafii->Region->ListRegion();
						?>
						]
					},
					yAxis: {min: 0,	title: {text: ''},labels: {formatter: function(){return Highcharts.numberFormat(this.value,0);}}},
					tooltip: {
						headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
						pointFormat	: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y}</b></td></tr>',
						footerFormat: '</table>',shared: true,useHTML: true
					},
					plotOptions: {column: {pointPadding: 0,borderWidth: 0}},
					exporting: {buttons: {contextButton: {menuItems: [{text: 'Print Chart',onclick: function(){this.print()}},
					{text: 'Export to PNG',onclick: function(){
						var x = prompt('Input file name','CHART_BAR_NASIONAL_<?=$Khafii->DTF->DTFFormat('dmY_His')?>');
						if(x != null){this.exportChart({filename:x})} 
						else{x = 'CHART_BAR_<?=$Region?>_<?=$Khafii->DTF->DTFFormat('dmY_His')?>'; this.exportChart({filename:x})}}
					}, 
					{text: 'Export to PDF',onclick: function(){
						var x = prompt('Input file name','CHART_BAR_NASIONAL_<?=$Khafii->DTF->DTFFormat('dmY_His')?>');
						if(x != null){this.exportChart({filename:x,type:"application/pdf"})} 
						else{x = 'CHART_BAR_<?=$Region?>_<?=$Khafii->DTF->DTFFormat('dmY_His')?>'; this.exportChart({filename:x,type:"application/pdf"})}}}]}
					},enabled:<?= $SExp[0] != 'Administrator' ? 'false' : 'true' ?>,sourceWidth:800,sourceHeight:600},
					series: [{name:'<?=next($Lang)?>',data:[
						<?php 
							$i = 1;
							$j = '';
							while($Result = $Khafii->Database->FetchAssoc($Parse)):
								$Cursor = $Khafii->Elector->ByRegion($i < 10 ? '0'.$i : $i);
								$Data	= $Khafii->Database->FetchAssoc($Cursor);
								$Kode	= $Data['KODE_REGION'] < 10 ? $Khafii->String->Replace('0','',$Data['KODE_REGION'],'R') : $Data['KODE_REGION'];
								$j += $Data['TOTAL_PRIA'];
								echo $Result['KODE_REGION'] == $Kode ? number_format($Data['TOTAL_PRIA'],0,',','') . ',' : 0 . ',';
							$i++;
							endwhile; 
							echo $j;
						?>
					]},
					{name: '<?=next($Lang)?>',data: [
						<?php 
							$i = 1;
							$j = '';
							$Parse	= $Khafii->Region->ListRegion();
							while($Result = $Khafii->Database->FetchAssoc($Parse)):
								$Cursor = $Khafii->Elector->ByRegion($i < 10 ? '0'.$i : $i);
								$Data	= $Khafii->Database->FetchAssoc($Cursor);
								$Kode	= $Data['KODE_REGION'] < 10 ? $Khafii->String->Replace('0','',$Data['KODE_REGION'],'R') : $Data['KODE_REGION'];
								$j += $Data['TOTAL_WANITA'];
								echo $Result['KODE_REGION'] == $Kode ? number_format($Data['TOTAL_WANITA'],0,',','') . ',' : 0 . ',';
							$i++;
							endwhile; 
							echo $j;
						?>
					]}]});
			});
			
			<?php 
				elseif(SELF_SUB_PARENT == 'chart.pie'): 
				$Lang	= $Khafii->InstantLanguage->Load();
				$Lang	= $Lang['ContentPie'];
				$Cursor = $Khafii->Elector->ByRegion('ALL');
				while($Data	= $Khafii->Database->FetchAssoc($Cursor)):
				if($Data['NAMA_REGION'] == 'AFRICA - EUROPA'):
					$Alias	= 'AFRICA / EUROPE';
					$Region = 'DIASPORA';
				elseif($Data['NAMA_REGION'] == 'BOLAMA - BIJAGOS'):
					$Alias	= 'BOLAMA';
					$Region = $Alias;
				elseif($Data['NAMA_REGION'] == 'SAB'):
					$Alias	= 'BISSAU';
					$Region	= $Alias;
				else:
					$Alias	= $Data['NAMA_REGION'];
					$Region	= $Alias;
				endif;
				$Array[]	= $Data['TOTAL_ELEKTOR'];
			?>
			
				$(function () {
					Highcharts.setOptions({colors: ['#CE1126','#009E49','#DDDF00','#24CBE5','#64E572','#FF9655','#FFF263','#6AF9C4']});
					Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function(color) {
						return {radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },stops: [[0, color],[1, Highcharts.Color(color).brighten(-0.3).get('rgb')]]};
					});
					$('#CHART<?=$Khafii->String->Charset($Region)?>').highcharts({
						chart: {plotBackgroundColor: null,plotBorderWidth: null,plotShadow: false},
						title: {text: '<?=current($Lang)?> : <b><?=$Khafii->String->Charset($Alias)?></b>'},
						tooltip: {pointFormat: '<b>{point.y} Elector</b>'},
						exporting: {buttons: {contextButton:{menuItems:[{text: 'Print Chart',onclick:function(){this.print()}},
						{text: 'Export to PNG',onclick: function(){
							var x = prompt('Input file name','CHART_PIE_<?=$Khafii->String->Charset($Region)?>_<?=$Khafii->DTF->DTFFormat('dmY_His')?>');
							if(x != null){this.exportChart({filename:x})} 
							else{x = 'CHART_PIE_<?=$Khafii->String->Charset($Region)?>_<?=$Khafii->DTF->DTFFormat('dmY_His')?>';
								this.exportChart({filename: x});
							}}
						}, 
						{text: 'Export to PDF',onclick: function(){
								var x = prompt('Input file name','CHART_PIE_<?=$Khafii->String->Charset($Region)?>_<?=$Khafii->DTF->DTFFormat('dmY_His')?>');
								if(x != null){this.exportChart({filename: x,type: "application/pdf"})} 
								else{x = 'CHART_PIE_<?=$Khafii->String->Charset($Region)?>_<?=$Khafii->DTF->DTFFormat('dmY_His')?>';
									this.exportChart({filename: x,type: "application/pdf"})}
								}}]}
						},enabled: <?= $SExp[0] != 'Administrator' ? 'false' : 'true' ?>,sourceWidth: 800,sourceHeight: 600},
						plotOptions: {
							pie: {allowPointSelect: true,cursor: 'pointer',dataLabels: {enabled: true,color: '#000000',connectorColor: '#000000',
									formatter: function(){return '<b>'+ this.point.name +'</b>: '+ Highcharts.numberFormat(this.y, 0, '.');}
								},showInLegend: true
							}
						},series: [{type: 'pie',name: this.x,data: [{name: '<?=$Lang['Content_Text1']?>',y: <?=$Data['TOTAL_PRIA']?>,},{name: '<?=$Lang['Content_Text2']?>',y: <?=$Data['TOTAL_WANITA']?>,}]
						}]
					});
				});
				
			<?php endwhile; ?>
				
				$(function () {
					Highcharts.setOptions({colors: ['#FFFF4F','#FFFFB9','#2200CC','#685C3F','#FFFF00','#B9D6FF','#FC9C00','#710067','#95008F','#FF4F4F']});
					Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function(color) {
						return {radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },stops: [[0, color],[1, Highcharts.Color(color).brighten(-0.3).get('rgb')]]};
					});
					$('#CHARTNASIONAL').highcharts({
						chart: {plotBackgroundColor: null,plotBorderWidth: null,plotShadow: false},
						title: {text: '<b><?=end($Lang)?></b>'},tooltip: {pointFormat: '<b>{point.y} Elector</b>'},
						exporting: {
							buttons: {
								contextButton: {
									menuItems: [{
										text: 'Print Chart',
										onclick: function() {
											this.print();	
										}
									},
									{
										text: 'Export to PNG',
										onclick: function() {
											var x = prompt('Input file name','CHART_PIE_NASIONAL_<?=$Khafii->DTF->DTFFormat('dmY_His')?>');
											if(x != null){
												this.exportChart({
													filename: x
												});
											} else {
												x = 'CHART_PIE_NASIONAL_<?=$Khafii->DTF->DTFFormat('dmY_His')?>';
												this.exportChart({
													filename: x
												});
											}
										}
									}, 
									{
										text: 'Export to PDF',
										onclick: function(){
											var x = prompt('Input file name','CHART_PIE_NASIONAL_<?=$Khafii->DTF->DTFFormat('dmY_His')?>');
											if(x != null){
												this.exportChart({
													filename: x,
													type: "application/pdf"
												});
											} else {
												x = 'CHART_PIE_NASIONAL_<?=$Khafii->DTF->DTFFormat('dmY_His')?>';
												this.exportChart({
													filename: x,
													type: "application/pdf"
												});
											}
										},
									}]	
								}
							},
							enabled: <?= $SExp[0] != 'Administrator' ? 'false' : 'true' ?>,
							sourceWidth: 800,
							sourceHeight: 600,
						},
						plotOptions: {
							pie: {allowPointSelect: true,cursor: 'pointer',dataLabels: {enabled: true,color: '#000000',connectorColor: '#000000',
									formatter: function(){return '<b>'+ this.point.name +'</b>: '+ Highcharts.numberFormat(this.y, 0, '.');}
								},showInLegend: true
							}
						},
						series: [{type: 'pie',name: this.x,
							data: [
							<?php 
								$R		= array();
								$Cursor = $Khafii->Elector->ByRegion('ALL');
								$Max	= max($Array);
								while($Result = $Khafii->Database->FetchAssoc($Cursor)):
							?>
								{
									name: '<?=$Khafii->String->Charset($Result['NAMA_REGION'])?>',
									y: <?=$Result['TOTAL_ELEKTOR']?>,
									sliced: <?=$Result['TOTAL_ELEKTOR'] == $Max ? 'true' : 'false'?>,
									selected: <?=$Result['TOTAL_ELEKTOR'] == $Max ? 'true' : 'false'?>,
								},								
							<?php endwhile; ?>
							]
						}]
					});
				});
				
			<?php 
				elseif(SELF_SUB_PARENT == 'chart.line'):
				$Lang	= $Khafii->InstantLanguage->Load();
				$Lang	= $Lang['ContentLine'];
				$Parse	= $Khafii->Region->ListRegion();
			?>
			
				$(function () {
					Highcharts.setOptions({colors: ['#CE1126','#3A009E','#DDDF00','#24CBE5','#64E572','#FF9655','#FFF263','#6AF9C4']});
					$('#CHART').highcharts({
						chart: {type: 'line'},title: {text: '<?=current($Lang)?>'},
						xAxis: {
							categories: [
								<?php 
								$Parse	= $Khafii->Region->ListRegion();
								while($Result	= $Khafii->Database->FetchAssoc($Parse)): 
								if($Result['NAMA'] == 'AFRICA - EUROPA'):
									$Alias	= 'AFR/EUR';
									$Region = $Alias;
								elseif($Result['NAMA'] == 'BOLAMA - BIJAGOS'):
									$Region	= 'BOLAMA';
								elseif($Result['NAMA'] == 'SAB'):
									$Alias	= 'BISSAU';
									$Region	= $Alias;
								else:
									$Alias	= $Result['NAMA'];
									$Region	= $Alias;
								endif;
								echo "'".$Khafii->String->Charset($Alias)."',";
								endwhile; 
								?>
							]
						},
						yAxis: {title: {text: ''},labels: {formatter: function(){return Highcharts.numberFormat(this.value,0);}}},
						tooltip: {
							enabled: true,
							formatter: function() {
								return '<b>'+ this.series.name +'</b><br/>'+
									this.x +': '+ this.y ;
							}
						},plotOptions: {line: {dataLabels: {enabled: true},enableMouseTracking: false}},
						exporting: {
							buttons: {
								contextButton: {
									menuItems: [{
										text: 'Print Chart',
										onclick: function() {
											this.print();	
										}
									},
									{
										text: 'Export to PNG',
										onclick: function() {
											var x = prompt('Input file name','CHART_LINE_NASIONAL_<?=$Khafii->DTF->DTFFormat('dmY_His')?>');
											if(x != null){
												this.exportChart({
													filename: x
												});
											} else {
												x = 'CHART_LINE_NASIONAL_<?=$Khafii->DTF->DTFFormat('dmY_His')?>';
												this.exportChart({
													filename: x
												});
											}
										}
									}, 
									{
										text: 'Export to PDF',
										onclick: function(){
											var x = prompt('Input file name','CHART_LINE_NASIONAL_<?=$Khafii->DTF->DTFFormat('dmY_His')?>');
											if(x != null){
												this.exportChart({
													filename: x,
													type: "application/pdf"
												});
											} else {
												x = 'CHART_LINE_NASIONAL_<?=$Khafii->DTF->DTFFormat('dmY_His')?>';
												this.exportChart({
													filename: x,
													type: "application/pdf"
												});
											}
										},
									}]	
								}
							},
							enabled: <?= $SExp[0] != 'Administrator' ? 'false' : 'true' ?>,
							sourceWidth: 800,
							sourceHeight: 600,
						},
						series: [{
							name: '<?=next($Lang)?>',
							data: [
								<?php 
									$i = 1;
									$Parse	= $Khafii->Region->ListRegion();
									while($Result	= $Khafii->Database->FetchAssoc($Parse)): 
										$Cursor = $Khafii->Elector->ByRegion($i < 10 ? '0'.$i : $i);
										$Data	= $Khafii->Database->FetchAssoc($Cursor);
										$Kode	= $Data['KODE_REGION'] < 10 ? $Khafii->String->Replace('0','',$Data['KODE_REGION'],'R') : $Data['KODE_REGION'];
										echo $Result['KODE_REGION'] == $Kode ? number_format($Data['TOTAL_ELEKTOR'],0,',','') . ',' : 0 . ',';
										$i++;
									endwhile; 
								?>
							]
						}]
					});
				});
			<?php endif; ?>
		</script>
	</body>
</html>