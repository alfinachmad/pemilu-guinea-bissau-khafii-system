<?php 
	if($this->Library->Handling->Set($_REQUEST['kode']) && SESSIONACTIVE == 'Administrator'):
		$Substr	= $_REQUEST['kode'] == NULL ? NULL : substr('000000000',0,9 - strlen($_REQUEST['kode'])) . $_REQUEST['kode'];
		$Kode	= $Substr; 
		$Name	= str_replace(' ','%',$_REQUEST['nama']); 
		$DOB	= $_REQUEST['dob']; 
		$Father = str_replace(' ','%',$_REQUEST['father']); 
		$Sub 	= $_REQUEST['submode']; 
		$Page 	= 15; $Numpage = '';
		$SQL 	= $this->Library->Elector->ByLookup($Kode,$Name,$DOB,$Father,'SQL'); 
		$Parse 	= $this->Library->Elector->ByLookup($Kode,$Name,$DOB,$Father); 
		$Numrow	= oci_fetch_all($Parse,$Result);
		(!$Sub ? $Sub = 1 : NULL); $Prev = $Sub - 1; $Next = $Sub + 1; $Start = (($Page * $Sub) - $Page);
		if($Numrow <= $Page):
			$Numpage 	= 1;
		elseif(($Numrow % $Page) == 0):
			$Numpage	= ($Numrow / $Page);
		else:
			$Numpage	= ($Numrow / $Page) + 1;
			$Numpage	= (int)$Numpage;
		endif;
		$End	= $Page * $Sub;
		($End > $Numrow ? $End = $Numrow : NULL);
?>
<table class="table table-striped b-t b-r b-l b-b" style="margin-bottom: 0px !important; padding-bottom:0px !important">
	<thead>
    	<tr>
        	<th style="text-align:center !important;">No. Elector</th>
        	<th style="text-align:center !important;">Name of Elector</th>
        	<th style="text-align:center !important;">Date of Birth</th>
        	<th style="text-align:center !important;">Name of Father</th>
        	<th style="text-align:center !important;">Sex</th>
        	<th style="text-align:center !important;">Action</th>
        </tr>
    </thead>
    <tbody>
    	<?php 
			for($i=$Start;$i<$End;$i++):
		?>
        <tr>
            <td class="b-r"><?=$Result['KODE_ELEKTOR'][$i]?></td>
            <td class="b-r" style="text-align:left !important;"><?=$this->Library->String->Charset($Result['NAMA'][$i])?></td>
            <td class="b-r"><?=$Result['TANGGAL_LAHIR'][$i]?></td>
            <td class="b-r" style="text-align:left !important;"><?=$this->Library->String->Charset($Result['NAMA_AYAH'][$i])?></td>
            <td class="b-r" style="text-align:left !important;"><?=$Result['KELAMIN_1'][$i] == 'WONEN' ? 'WOMEN' : $Result['KELAMIN_1'][$i]?></td>
            <td>
                <div class="checkbox" style="margin-top:0px !important; margin-bottom:0px !important;">
                    <label>
                        <input name="select" type="checkbox" value="<?=$Result['KODE_ELEKTOR'][$i]?>"> Select
                    </label>
                </div>
            </td>
        </tr>
        <?php endfor; ?>
    </tbody>
</table>
<div class="text-center"> 
    <ul class="pagination pagination"> 
    	<?php if($Prev): ?>
            <li><a href="<?=REALURL?>&submode=<?=$Prev?>"><i class="icon-chevron-left"></i></a></li> 
        <?php endif; ?>
        <?php for($i=1;$i<=$Numpage;$i++): ?>
        	<?php if($i != $Sub): ?>
                <li><a href="<?=REALURL?>&submode=<?=$i?>"><?=$i?></a></li>
            <?php else: ?>
                <li><a><?=$i?></a></li>
            <?php endif; ?> 
        <?php endfor; ?>
        <?php if($Sub != $Numpage): ?>
            <li><a href="<?=REALURL?>&submode=<?=$Next?>"><i class="icon-chevron-right"></i></a></li> 
    	<?php endif; ?>
    </ul> 
</div> 
<?php else: $this->Library->InstantControll->Redirect(ERROR404); endif; ?>