<?php
	$Lang	= $this->Library->InstantLanguage->Load();
	$Lang	= $Lang['ElectInfo'];
	if(SESSIONACTIVE == 'Guest'):
		$this->Library->InstantControll->Redirect(SELF_PARENT.'@disallowed_'.$this->Library->Security->Encrypt(SELF_SUB_PARENT.'@@'.$this->Library->DTF->DTFFormat('l, d F Y H:i:s','R')));
	endif;
	if($_SESSION['modeprofile'] == 'Active'): echo ''; else: 
?>
<section class="panel">
	<header id="header" class="panel-heading">
    	Information of Elector
        <span class="pull-right">Last Updated :
        	<span class="dfn">
				<?=$this->Library->InstantInfo->GetLastUpdate(basename(__DIR__),'d F Y - H:i:s');?>
            </span>
        </span>
    </header>
    <div class="panel-body">
        <form method="post" id="formElector" class="form-horizontal">
            <div class="form-group"> 
                <label class="col-sm-1 control-label text-left" >No. Elector</label> 
                <div class="col-sm-1 m-t-none"> 
                    <input type="text" name="nim" class="nim bg-focus form-control input-s-sm text-right" maxlength="9" onKeyPress="return InputAngka(event)" autofocus>
                </div>
                <div class="col-sm-1 m-t-none"> 
                    <input name="dob" type="text" id="datepicker" class="dob form-control input-s-sm inline text-center" placeholder="Date of Birth">
                </div>
            </div>   
            <div class="form-group"> 
                <label class="col-sm-1 control-label text-left" >Name</label> 
                <div class="col-sm-4 m-t-none" style="padding-right:0px !important;"> 
                    <input type="text" name="name" class="name form-control text-left">
                </div>
                <div class="col-sm-4 m-t-none"> 
                    <input type="text" name="father" class="father form-control text-left" placeholder="Name of Father">
                </div>
                <button type="submit" name="submit" id="idFormElector" class="btn btn-success"><i class="icon-search"></i></button> 
            </div> 
        </form>
        <!-- =================================================================== MODAL -->
        <form method="post" id="dialogFormElector">
            <div class="modal fade bs-modal-lg" id="dialogInfoElector" tabindex="-1" role="dialog" aria-hidden="true" >
                <div class="modal-dialog modal-lg" style="width: 85% !important">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" onclick="window.location.href='<?=REALURL?>'">
                                &times;
                            </button>
                            <h4 class="modal-title" id="myLargeModalLabel">
                                Advanced Search of Elector 
                                <img id="load" src="<?=Image?>loading" style="width: 24px !important; height:24px !important; margin-top:0px !important;">
                            </h4>
                        </div>
                        <div class="modal-body" style="padding-top:0px !important; padding-bottom:0px !important; padding-left:0px !important; padding-right:0px !important;">
                            <div id="bodyElector" class="panel-body text-center">
                                <div id="dataElector" class="table-responsive">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" onclick="window.location.href='<?=REALURL?>'">Close</button>
                            <button type="submit" name="submit" id="formDialog" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- =================================================================== END MODAL -->
    </div>
</section>	
<?php
endif;
if($this->Library->Handling->Set($_POST['submit'])):
	$Cursor		= $this->Library->Elector->ByNumber($_POST['select'] == NULL ? substr('000000000',0,9 - strlen($_POST['nim'])) . $_POST['nim'] : $_POST['select']);
	$Result		= $this->Library->Database->FetchAssoc($Cursor);
	$Photo		= @$this->Library->Security->Base64Enc($Result['PHOTO']->load());
	$Sign		= @$this->Library->Security->Base64Enc($Result['SIGN']->load());
	#print_r($Result);
?>
<section id="contentbody" class="panel">
    <div class="panel-body">
    	<form method="post" class="form-horizontal">
            <div class="pull-left col-sm-6" style="padding-left: 0px !important;">
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >No. Elector</label> 
                    <div class="col-sm-3 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control input-s-sm text-right" value="<?=$Result['KODE_ELEKTOR']?>" readonly>
                    </div>
                </div>   
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >No. Registration</label> 
                    <div class="col-sm-3 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control input-s-sm text-right" value="<?=$Result['NO_INPUT']?>" readonly>
                    </div>
                </div>   
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Name</label> 
                    <div class="col-sm-8 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['NAMA'])?>" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Sex</label> 
                    <div class="col-sm-4 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control input-s-sm" value="<?=$this->Library->String->Charset($Result['KELAMIN_1'])?>" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Date of Birth</label> 
                    <div class="col-sm-4 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control input-s-sm" value="<?=$Result['TANGGAL_LAHIR']?>" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Marital Status</label> 
                    <div class="col-sm-4 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['STATUS_KAWIN_1'])?>" readonly>
                    </div>
                </div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Nationality</label> 
                    <div class="col-sm-4 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="GUINEA-BISSAU" readonly>
                    </div>
                </div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Residence</label> 
                    <div class="col-sm-4 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['ALAMAT'])?>" readonly>
                    </div>
                </div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Occupation</label> 
                    <div class="col-sm-8 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['KODE_PROFESI'])?>" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Father</label> 
                    <div class="col-sm-8 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['NAMA_AYAH'])?>" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Mother</label> 
                    <div class="col-sm-8 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['NAMA_IBU'])?>" readonly>
                    </div>
                </div>   
            </div>
            <div class="pull-right col-sm-6">
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Photo</label> 
                    <div class="col-sm-5 media m-t-none"> 
                        <div class="bg-light pull-left text-center thumb-lg" style="border-radius:3px !important; border:1px solid #999 !important;">
                        	<img src="data:image/png;base64,<?=$Photo?>" style="width:85px !important; height:113px !important;"/>
                        </div>
                        <div class="bg-light pull-left text-center thumb-lg" style="border-radius:3px !important; border:1px solid #999 !important;">
                        </div>
                    </div> 
                </div>   
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Region</label> 
                    <div class="col-sm-5 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['NAMA_REGION'])?>" readonly>
                    </div>
                </div>   
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Sector</label> 
                    <div class="col-sm-5 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['NAMA_SEKTOR'])?>" readonly>
                    </div>
                </div>   
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Electoral District</label> 
                    <div class="col-sm-5 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['NAMA_ALDEIA'])?>" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Table</label> 
                    <div class="col-sm-9 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['NAMA_DE'])?>" readonly>
                    </div>
                </div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Date of Input</label> 
                    <div class="col-sm-3 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control text-center" value="<?=$Result['TANGGAL_INPUT']?>" readonly>
                    </div>
                </div> 
                <div class="form-group"> 
                	<table class="table table-striped b-t b-r b-l b-b" style="margin-bottom: 5px !important; padding-bottom:0px !important">
                    	<thead>
                        	<tr>
                            	<th style="text-align:center !important;">Region</th>
                                <th style="text-align:center !important;">Constituency</th>
                                <th style="text-align:center !important;">Sector</th>
                                <th style="text-align:center !important;">Electoral District</th>
                                <th style="text-align:center !important;">MRE/MAV</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<tr>
                            	<td class="text-center b-r"><?=$Result['KODE_REGION']?></td>
                                <td class="text-center b-r"><?=$Result['URUT_CIRCULO']?></td>
                                <td class="text-center b-r"><?=$Result['URUT_SEKTOR']?></td>
                                <td class="text-center b-r"><?=$Result['URUT_DE']?></td>
                                <td class="text-center"><?=$Result['KODE_MEJA_NUMBER']?></td>
                            </tr>
                        </tbody>
                    </table>
                	<table class="table table-striped b-t b-r b-l b-b" style="margin-bottom: 5px !important; padding-bottom:0px !important">
                    	<thead>
                        	<tr>
                            	<th style="text-align:center !important;">PC</th>
                                <th style="text-align:center !important;">Naturalization</th>
                                <th style="text-align:center !important;">ID Elector</th>
                                <th style="text-align:center !important;">No. ID</th>
                                <th style="text-align:center !important;">No. SEGEL</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<tr>
                            	<td class="text-center b-r"><?=$Result['NAMA_KOMPUTER']?></td>
                                <td class="text-center b-r"><?=$Result['NATURALISASI_1']?></td>
                                <td class="text-center b-r"><?=$Result['IDENTITAS_ELEKTOR_2']?></td>
                                <td class="text-center b-r"><?=$Result['NO_IDENTITAS']?></td>
                                <td class="text-center b-r"><?=$Result['NO_SEGEL']?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
</section>
<?php 
	else: if($_SESSION['modeprofile'] == 'Active'): 
	$Cursor		= $this->Library->Elector->ByNumber(IDPROFILE);
	$Result		= $this->Library->Database->FetchAssoc($Cursor);
	$Photo		= $this->Library->Security->Base64Enc($Result['PHOTO']->load());
	$Sign		= $this->Library->Security->Base64Enc($Result['SIGN']->load());
?>
<section id="contentbody" class="panel">
	<header id="header" class="panel-heading">
    	<h1>
			<?=$this->Library->String->Charset($Result['KELAMIN_1']) == 'MEN' ? $Lang['GenderMan'] : $Lang['GenderWoman']?> 
			<?=ucwords(strtolower($this->Library->String->Charset($Result['NAMA'])))?>
        </h1>
    </header>
    <div class="panel-body">
    	<form method="post" class="form-horizontal">
            <div class="pull-left col-sm-6" style="padding-left: 0px !important;">
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >No. Elector</label> 
                    <div class="col-sm-3 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control input-s-sm text-right" value="<?=$Result['KODE_ELEKTOR']?>" readonly>
                    </div>
                </div>   
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >No. Registration</label> 
                    <div class="col-sm-3 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control input-s-sm text-right" value="<?=$Result['NO_INPUT']?>" readonly>
                    </div>
                </div>   
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Name</label> 
                    <div class="col-sm-8 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['NAMA'])?>" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Sex</label> 
                    <div class="col-sm-4 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control input-s-sm" value="<?=$this->Library->String->Charset($Result['KELAMIN_1'])?>" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Date of Birth</label> 
                    <div class="col-sm-4 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control input-s-sm" value="<?=$Result['TANGGAL_LAHIR']?>" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Marital Status</label> 
                    <div class="col-sm-4 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['STATUS_KAWIN_1'])?>" readonly>
                    </div>
                </div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Nationality</label> 
                    <div class="col-sm-4 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="GUINEA-BISSAU" readonly>
                    </div>
                </div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Residence</label> 
                    <div class="col-sm-4 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['ALAMAT'])?>" readonly>
                    </div>
                </div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Occupation</label> 
                    <div class="col-sm-8 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['KODE_PROFESI'])?>" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Father</label> 
                    <div class="col-sm-8 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['NAMA_AYAH'])?>" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Mother</label> 
                    <div class="col-sm-8 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['NAMA_IBU'])?>" readonly>
                    </div>
                </div>   
            </div>
            <div class="pull-right col-sm-6">
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Photo</label> 
                    <div class="col-sm-5 media m-t-none"> 
                        <div class="bg-light pull-left text-center thumb-lg" style="border-radius:3px !important; border:1px solid #999 !important;">
                        	<img src="data:image/png;base64,<?=$Photo?>" style="width:85px !important; height:113px !important;"/>
                        </div>
                        <div class="bg-light pull-left text-center thumb-lg" style="border-radius:3px !important; border:1px solid #999 !important;">
                        </div>
                    </div> 
                </div>   
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Region</label> 
                    <div class="col-sm-5 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['NAMA_REGION'])?>" readonly>
                    </div>
                </div>   
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Sector</label> 
                    <div class="col-sm-5 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['NAMA_SEKTOR'])?>" readonly>
                    </div>
                </div>   
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Electoral District</label> 
                    <div class="col-sm-5 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['NAMA_ALDEIA'])?>" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Table</label> 
                    <div class="col-sm-9 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="<?=$this->Library->String->Charset($Result['NAMA_DE'])?>" readonly>
                    </div>
                </div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Date of Input</label> 
                    <div class="col-sm-3 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control text-center" value="<?=$Result['TANGGAL_INPUT']?>" readonly>
                    </div>
                </div> 
                <div class="form-group"> 
                	<table class="table table-striped b-t b-r b-l b-b" style="margin-bottom: 5px !important; padding-bottom:0px !important">
                    	<thead>
                        	<tr>
                            	<th style="text-align:center !important;">Region</th>
                                <th style="text-align:center !important;">Constituency</th>
                                <th style="text-align:center !important;">Sector</th>
                                <th style="text-align:center !important;">Electoral District</th>
                                <th style="text-align:center !important;">MRE/MAV</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<tr>
                            	<td class="text-center b-r"><?=$Result['KODE_REGION']?></td>
                                <td class="text-center b-r"><?=$Result['URUT_CIRCULO']?></td>
                                <td class="text-center b-r"><?=$Result['URUT_SEKTOR']?></td>
                                <td class="text-center b-r"><?=$Result['URUT_DE']?></td>
                                <td class="text-center"><?=$Result['KODE_MEJA_NUMBER']?></td>
                            </tr>
                        </tbody>
                    </table>
                	<table class="table table-striped b-t b-r b-l b-b" style="margin-bottom: 5px !important; padding-bottom:0px !important">
                    	<thead>
                        	<tr>
                            	<th style="text-align:center !important;">PC</th>
                                <th style="text-align:center !important;">Naturalization</th>
                                <th style="text-align:center !important;">ID Elector</th>
                                <th style="text-align:center !important;">No. ID</th>
                                <th style="text-align:center !important;">No. Segel</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<tr>
                            	<td class="text-center b-r"><?=$Result['NAMA_KOMPUTER']?></td>
                                <td class="text-center b-r"><?=$Result['NATURALISASI_1']?></td>
                                <td class="text-center b-r"><?=$Result['IDENTITAS_ELEKTOR_2']?></td>
                                <td class="text-center b-r"><?=$Result['NO_IDENTITAS']?></td>
                                <td class="text-center b-r"><?=$Result['NO_SEGEL']?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
</section>
<?php else: ?>
<section id="contentbody" class="panel">
    <div class="panel-body">
    	<form method="post" class="form-horizontal">
            <div class="pull-left col-sm-6" style="padding-left: 0px !important;">
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >No. Elector</label> 
                    <div class="col-sm-5 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control input-s-sm" readonly>
                    </div>
                </div>   
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >No. Registration</label> 
                    <div class="col-sm-5 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control input-s-sm" readonly>
                    </div>
                </div>   
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Name</label> 
                    <div class="col-sm-8 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Sex</label> 
                    <div class="col-sm-4 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control input-s-sm" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Date of Birth</label> 
                    <div class="col-sm-4 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control input-s-sm" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Marital Status</label> 
                    <div class="col-sm-4 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" readonly>
                    </div>
                </div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Nationality</label> 
                    <div class="col-sm-4 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" readonly>
                    </div>
                </div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Residence</label> 
                    <div class="col-sm-4 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" readonly>
                    </div>
                </div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Occupation</label> 
                    <div class="col-sm-8 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Father</label> 
                    <div class="col-sm-8 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Mother</label> 
                    <div class="col-sm-8 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" readonly>
                    </div>
                </div>   
            </div>
            <div class="pull-right col-sm-6">
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Photo</label> 
                    <div class="col-sm-4 media m-t-none"> 
                        <div class="bg-light pull-left text-center media-lg thumb-lg padder-v" style="border-radius:3px !important;">
                        	<i class="icon-user inline icon-light icon-3x m-t-lg m-b-lg"></i>
                        </div>
                    </div> 
                </div>   
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Region</label> 
                    <div class="col-sm-5 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="" readonly>
                    </div>
                </div>   
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Sector</label> 
                    <div class="col-sm-5 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="" readonly>
                    </div>
                </div>   
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Electoral District</label> 
                    <div class="col-sm-5 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="" readonly>
                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Table</label> 
                    <div class="col-sm-9 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="" readonly>
                    </div>
                </div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label text-left" >Date of Input</label> 
                    <div class="col-sm-3 media m-t-none"> 
                        <input type="text" name="data[]" class="bg-focus form-control" value="" readonly>
                    </div>
                </div> 
                <div class="form-group"> 
                	<table class="table table-striped b-t b-r b-l b-b" style="margin-bottom: 0px !important; padding-bottom:0px !important">
                    	<thead>
                        	<tr>
                            	<th style="text-align:center !important;">Region</th>
                                <th style="text-align:center !important;">Constituency</th>
                                <th style="text-align:center !important;">Sector</th>
                                <th style="text-align:center !important;">Electoral District</th>
                                <th style="text-align:center !important;">MRE/MAV</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<tr>
                            	<td class="text-center b-r"></td>
                                <td class="text-center b-r"></td>
                                <td class="text-center b-r"></td>
                                <td class="text-center b-r"></td>
                                <td class="text-center"></td>
                            </tr>
                        </tbody>
                    </table>
                	<table class="table table-striped b-t b-r b-l b-b" style="margin-bottom: 5px !important; padding-bottom:0px !important">
                    	<thead>
                        	<tr>
                            	<th style="text-align:center !important;">PC</th>
                                <th style="text-align:center !important;">Naturalization</th>
                                <th style="text-align:center !important;">ID Elector</th>
                                <th style="text-align:center !important;">No. ID</th>
                                <th style="text-align:center !important;">No. Segel</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<tr>
                                <td class="text-center b-r"></td>
                                <td class="text-center b-r"></td>
                                <td class="text-center b-r"></td>
                                <td class="text-center b-r"></td>
                                <td class="text-center b-r"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
</section>
<?php endif; endif; ?>