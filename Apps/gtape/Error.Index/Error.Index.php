<section id="content"> 
    <div class="row m-n"> 
        <div class="col-sm-4 col-sm-offset-4"> 
            <div class="text-center m-b-lg"> 
                <h1 class="text-white animated bounceInDown" style="color:#009E49 !important;font-size: 180px !important; font-weight:normal !important;">404</h1> 
                <h3>Ooh, So sad! Page not found.</h3>
            </div> 
            <div class="list-group m-b-sm bg-white m-b-lg"> 
                <a href="<?=SELF_PARENT?>" class="list-group-item"> 
                    <i class="icon-chevron-right"></i><i class="icon-home"></i> Back to Homepage 
                </a>
                <a onclick="javascript:history.back(-1)" class="list-group-item"> 
                    <i class="icon-chevron-right"></i> 
                    <i class="icon-question"></i> Back to previous page 
                </a> 
            </div> 
        </div> 
    </div> 
</section> 
<footer id="footer"> 
    <div class="text-center padder clearfix"> 
        <p> 
        	<small><?=$this->Library->Config['App']['Description']?><br>&copy; <?=$this->Library->DTF->DTFFormat('Y')?></small> 
        </p> 
    </div> 
</footer>
