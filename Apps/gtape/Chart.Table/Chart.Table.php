<?php
	$Sess	= $_SESSION[SELF_PARENT];
	$SExp	= $this->Library->String->Exploding($this->Library->Security->Base64Dec($Sess),'||||');
	$Lang	= $this->Library->InstantLanguage->Load();
	$Lang	= $Lang['Table'];
?>
<section id="toppanel" class="panel">
	<header id="header" class="panel-heading">
    	<?=$Lang['Left_Text_Content']?> <label class="label bg-danger m-l-xs">Table Chart</label>
        <span class="pull-right"><?=next($Lang)?> : 
        	<span class="dfn">
				<?=$this->Library->InstantInfo->GetLastUpdate(basename(__DIR__),'d F Y - H:i:s');?>
            </span>
        </span>
    </header>
	
    <section id="input" class="panel" style="border: none !important;">
    	<div class="panel-body">
            <form method="post" class="form-horizontal">
            	<label class="m-r-sm"><?=next($Lang)?> : </label>
                <input name="dateregion" type="text" id="datepicker" class="input-sm form-control input-s-sm inline text-center datePost" style="width: 125px !important;" value="<?=date('d-m-Y',strtotime($this->Library->Elector->ByDate()))?>"> 
                <select name="param" class="form-control input-sm input-s-sm inline paramPost"> 
                    <option value="2">Greater than</option>
                    <option value="0">Less than</option>
                    <option value="1" selected>Equal</option>
                </select>
                <select name="age" class="form-control input-sm input-s-sm inline agePost" style="width: 60px !important;"> 
                	<?php for($i=0;$i<101;$i++): ?>
                    <option <?=$i==18?'selected':''?>><?=$i?></option> 
                    <?php endfor; ?>
                </select>
                <button type="button" name="submit" class="btn btn-sm btn-white openResult" style="display: inline !important;">
                	<i class="icon-repeat"></i>
                </button> 
                <div id="postLoad" style="display:none;">
                	<img src="<?=Image?>loading" style="width: 24px !important; height:24px !important; margin-top:0px !important; margin-left:5px; margin-right:5px !important;">
                    Loading . . . . .
                </div>
            </form>
    	</div>
    </section>
</section>
<style>
	@media print {
		aside.bg-primary, #input, header.header, header#panel, ul.nav, ul.nav-tabs {
			display: none !important;	
		}
	}
</style>
<section id="contentbody" class="panel">
	<header id="panel" class="panel-heading">
    	<?=next($Lang)?>
        <?php if($SExp[0] != 'Administrator'): ?>
        
        <?php else: ?>
        <span class="pull-right"><?=next($Lang)?> :
        	<a href="javascript:window.print()"><i class="icon-print"></i></a>
        </span>
        <?php endif; ?>
    </header>
    <div class="panel-body" style="padding-top:0px !important; padding-left: 0px !important;">
    	<div class="tab-content">
			<?php
				$Lang	= $this->Library->InstantLanguage->Load();
				$Lang	= $Lang['TableC1'];
			?>
			<div class="tab-pane fade active in" id="SEXO">
            	<div class="panel-body text-center">
                    <div class="table-responsive">
                        <table class="table table-striped b-t" style="margin-bottom: 0px !important; padding-bottom:0px !important">
                            <thead>
                                <tr>
                                    <th class="v-middle fc-header-center b-l" width="45" rowspan="2">
                                        <?=current($Lang)?>
                                    </th>
                                    <th class="v-middle fc-header-center" rowspan="2">
                                        <?=next($Lang)?>
                                    </th>
                                    <th class="fc-header-center" colspan="2">
                                        <?=next($Lang)?>
                                    </th>
                                    <th class="fc-header-center" colspan="2">
                                        <?=next($Lang)?>
                                    </th>
                                    <th class="fc-header-center" style="border-right:1px solid #E0E4E8 !important;" colspan="2">
                                        <?=next($Lang)?>
                                    </th>
                                </tr>
                                <tr>
                                    <th class="fc-header-center"><?=next($Lang)?></th>
                                    <th class="fc-header-center"><?=next($Lang)?></th>
                                    <th class="fc-header-center"><?=next($Lang)?></th>
                                    <th class="fc-header-center"><?=next($Lang)?></th>
                                    <th class="fc-header-center"><?=next($Lang)?></th>
                                    <th class="fc-header-center" style="border-right:1px solid #E0E4E8 !important;"><?=next($Lang)?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $Parse 	= $this->Library->Region->ListRegion();
                                    $this->Library->Database->Execute(
                                        array($Parse)
                                    );
                                    while($Result = $this->Library->Database->FetchAssoc($Parse)):
                                ?>
                                <tr>
                                    <td align="center" class="b-r b-l">
                                        <?=$Result['KODE_REGION']?>
                                    </td>
                                    <td align="left" class="b-r">
                                        <?=$this->Library->String->Charset($Result['NAMA'])?>
                                    </td>
                                    <td align="right" id="mt<?=$Result['KODE_REGION']?>" class="b-r">
                                        0
                                    </td>
                                    <td align="right" id="mp<?=$Result['KODE_REGION']?>" class="b-r">
                                        0
                                    </td>
                                    <td align="right" id="wt<?=$Result['KODE_REGION']?>" class="b-r">
                                        0
                                    </td>
                                    <td align="right" id="wp<?=$Result['KODE_REGION']?>" class="b-r">
                                        0
                                    </td>
                                    <td align="right" id="tt<?=$Result['KODE_REGION']?>" class="b-r">
                                        0
                                    </td>
                                    <td align="right" id="tp<?=$Result['KODE_REGION']?>" class="b-r">
                                        0 
                                    </td>
                                </tr>
                                <?php endwhile; ?>
                                <tr>
                                    <td align="center" colspan="2" class="b-r b-b b-l"><strong><?=next($Lang)?></strong></td>
                                    <td align="right" id="mtt" class="b-r b-b"><strong>0</strong></td>
                                    <td align="right" id="mtp" class="b-b b-r"><strong>0</strong></td>
                                    <td align="right" id="wtt" class="b-r b-b"><strong>0</strong></td>
                                    <td align="right" id="wtp" class="b-b b-r"><strong>0</strong></td>
                                    <td align="right" id="ttt" class="b-b b-r"><strong>0</strong></td>
                                    <td align="right" id="ttp" class="b-b b-r"><strong>0</strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php
			$Lang	= $this->Library->InstantLanguage->Load();
			$Lang	= $Lang['TableC2'];
			?>
            <div class="tab-pane fade" id="AGE">
            	<div class="panel-body text-center">
                    <div class="table-responsive">
                        <div class="table-responsive">
                            <table class="table table-striped b-t" style="margin-bottom: 0px !important; padding-bottom:0px !important">
                                <thead>
                                    <tr>
                                        <th class="v-middle fc-header-center b-l" rowspan="2"><?=current($Lang)?></th>
                                        <th class="v-middle fc-header-center" rowspan="2"><?=next($Lang)?></th>
                                        <th class="v-middle fc-header-center" colspan="4"><?=next($Lang)?></th>
                                        <th class="v-middle fc-header-center b-r" rowspan="2"><?=next($Lang)?></th>
                                    </tr>
                                    <tr>
                                        <th class="v-middle fc-header-center"><?=next($Lang)?></th>
                                        <th class="v-middle fc-header-center"><?=next($Lang)?></th>
                                        <th class="v-middle fc-header-center"><?=next($Lang)?></th>
                                        <th class="v-middle fc-header-center"><?=next($Lang)?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php
                                    $Parse = $this->Library->Region->ListRegion();
									$i = 1;
									while($Result = $this->Library->Database->FetchAssoc($Parse)):
									?>
                                    <tr>
                                        <td class="b-l b-r" align="center"><?=$i?></td>
                                        <td class="b-r" align="left"><?=$this->Library->String->Charset($Result['NAMA'])?></td>
                                        <td id="ay<?=$Result['KODE_REGION']?>" class="b-r" align="right">0</td>
                                        <td id="aa<?=$Result['KODE_REGION']?>" class="b-r" align="right">0</td>
                                        <td id="ag<?=$Result['KODE_REGION']?>" class="b-r" align="right">0</td>
                                        <td id="ao<?=$Result['KODE_REGION']?>" class="b-r" align="right">0</td>
                                        <td id="at<?=$Result['KODE_REGION']?>" class="b-r" align="right">0</td>
                                    </tr>
                                    <?php $i++; endwhile; ?>
                                    <tr>
                                        <td align="center" colspan="2" class="b-r b-b b-l"><strong><?=next($Lang)?></strong></td>
                                        <td id="tay" align="right" class="b-r b-b"><strong>0</strong></td>
                                        <td id="taa" align="right" class="b-r b-b"><strong>0</strong></td>
                                        <td id="tag" align="right" class="b-r b-b"><strong>0</strong></td>
                                        <td id="tao" align="right" class="b-r b-b"><strong>0</strong></td>
                                        <td id="tat" align="right" class="b-r b-b"><strong>0</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        <?php $this->Library->Database->FreeStatement($Parse); ?>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <div class="panel-heading bg-light">
    	<ul class="nav nav-tabs nav-justified">
        	<li class="active">
				<a href="#SEXO" data-toggle="tab"><?=next($Lang)?></a>
            </li>
            <li>
				<a href="#AGE" data-toggle="tab"><?=next($Lang)?></a>
            </li>
        </ul>
    </div>
</section>