<style>
	@media print {
		aside.bg-primary, #input, header.header, header#panel, ul.nav, ul.nav-tabs {
			display: none !important;	
		}
	}
</style>
<section id="toppanel" class="panel">
	<header id="header" class="panel-heading">
    	Recapitulation of Aproximation Total Elector Guinea-Bissau <label class="label bg-danger m-l-xs">Table Chart</label>
        <span class="pull-right text-sm">Las Updated : 
        	<span class="dfn">
				<?=$this->Library->InstantInfo->GetLastUpdate(basename(__DIR__),'d F Y - H:i:s');?>
            </span>
        </span>
    </header>
    <div class="panel-body">
        <table class="table table-striped b-t b-b b-r">
            <thead>
                <tr>
                    <th class="v-middle fc-header-center b-l" rowspan="2">No.</th>
                    <th class="v-middle fc-header-center" rowspan="2">Region</th>
                    <th class="fc-header-center b-r" colspan="2">Total</th>
                    <th class="fc-header-center b-r" colspan="2">Percent (%)</th>
                </tr>
                <tr>
                    <th class="fc-header-center">Aproximation</th>
                    <th class="fc-header-center">Realization</th>
                    <th class="fc-header-center">Aproximation</th>
                    <th class="fc-header-center">Realization</th>
                </tr>
			</thead>
            <tbody>
				<?php
					$i	= 0;
					$j	= 0;
					$k	= 0;
					$l	= 0;
                    $Cursor = $this->Library->Elector->ByEstimate();
                    while($Result = $this->Library->Database->FetchAssoc($Cursor)):
						$i += $Result['TOTAL_ALL'];
						$j += $Result['PERSEN_ALL'];
						$k += $Result['TOTAL'];
						$l += $Result['PERSEN'];
                ?>
                <tr>
                    <td align="center" class="b-r b-l b-t"><?=$Result['KODE_REGION']?></td>
                    <td align="left" class="b-r b-t"><?=$Result['NAMA']?></td>
                    <td align="right" class="b-r b-t"><?=$this->Library->String->NumFormat($Result['TOTAL'],0)?></td>
                    <td align="right" class="b-r b-t"><?=$this->Library->String->NumFormat($Result['TOTAL_ALL'],0)?></td>
                    <td align="right" class="b-r b-t"><?=$this->Library->String->NumFormat($Result['PERSEN_ALL'],2)?></td>
                    <td align="right" class="b-r b-t"><?=$this->Library->String->NumFormat($Result['PERSEN'],2)?></td>
                </tr>
                <?php endwhile; ?>
                <tr>
                    <td align="center" colspan="2" class="b-r b-b b-l"><strong>TOTAL ELECTOR</strong></td>
                    <td align="right" class="b-r b-b"><strong><?=$this->Library->String->NumFormat($k)?></strong></td>
                    <td align="right" class="b-b b-r"><strong><?=$this->Library->String->NumFormat($i)?></strong></td>
                    <td align="right" class="b-b b-r"><strong><?=$this->Library->String->NumFormat($j,2)?></strong></td>
                    <td align="right" class="b-b b-r"><strong>-</strong></td>
                </tr>
        	</tbody>
        </table>
    </div>
</section>