<?php
	if($this->Library->Handling->Set($_REQUEST['date'])):
		$Open	= $this->Library->FileSystem->FileGet($this->Library->Config['json']['src'],true);
		$Decode	= $this->Library->Security->JSONDec($Open,true);
		$TotalDataByUmur	= $Decode[10]['TOTAL_ELEKTOR'] + $Decode[11]['TOTAL_ELEKTOR'] + $Decode[12]['TOTAL_ELEKTOR'] + $Decode[13]['TOTAL_ELEKTOR'] 
							+ $Decode[14]['TOTAL_ELEKTOR'] + $Decode[15]['TOTAL_ELEKTOR'] + $Decode[16]['TOTAL_ELEKTOR'] + $Decode[17]['TOTAL_ELEKTOR']
							+ $Decode[18]['TOTAL_ELEKTOR'] + $Decode[19]['TOTAL_ELEKTOR'];   
		$GetTotalElektor	= $this->Library->Elector->ByRegion('ALL',NULL,'TOTAL');
		$Cursor	= $this->Library->Elector->ByRegion('ALL',
			array($_REQUEST['date'],$_REQUEST['age'],$_REQUEST['param'])
		);
		$Result = $this->Library->Database->FetchAssoc($Cursor);
		if($Result['TOTAL_ALL'] == $Decode[0]['TOTAL_ALL'] && count($Open) != 0):
			NULL;
		else:
			$Cursor	= $this->Library->Elector->ByRegion('ALL',array($_REQUEST['date'],$_REQUEST['age'],$_REQUEST['param']));
			while($Result = $this->Library->Database->FetchAssoc($Cursor)):
				$Data[]	= $Result;
			endwhile;
			$Cursor	= $this->Library->Elector->ByAge('ALL',$_REQUEST['date']);
			while($Result = $this->Library->Database->FetchAssoc($Cursor)):
				$Data[]	= $Result;
			endwhile;
			$this->Library->FileSystem->FilePut($this->Library->Config['json']['src'],$this->Library->Security->JSONEnc($Data));
		endif;
	else:
		$this->Library->InstantControll->Redirect(ERROR404);
	endif;
?>