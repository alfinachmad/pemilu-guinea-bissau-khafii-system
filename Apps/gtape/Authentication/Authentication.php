<?php 
	$Lang	= $this->Library->InstantLanguage->Load();
	$Lang	= $Lang['Login'];
	$this->Library->FileSystem->FilePut($this->Library->Config['json']['src'],'NULL');
?>
<section id="content" class="m-t-lg wrapper-md animated fadeInDown"> 
	<a class="nav-brand" href="<?=SELF_PARENT?>">
    	<img src="<?=Image?>avatar" width="175" height="175"> 
    </a> 
    <div class="row m-n"> 
    	<div class="col-md-4 col-md-offset-4 m-t-lg"> 
        	<section class="panel"> 
            	<header class="panel-heading text-center"> <?=$Lang['Center_Text']?> </header> 
                <form method="post" class="panel-body"> 
                	<div class="form-group"> 
                    	<label class="control-label"><?=$Lang['First_Row']?></label> 
                        <input type="text" name="username" id="inputUsername" placeholder="<?=$Lang['First_Row']?>" class="form-control" autofocus required> 
                    </div> 
                    <div class="form-group"> 
                    	<label class="control-label"><?=$Lang['Second_Row']?></label> 
                        <input type="password" name="password" id="inputPassword" placeholder="<?=$Lang['Second_Row']?>" class="form-control" required> 
                    </div> 
                    <div class="checkbox"> 
                    	<label> 
                        <input type="checkbox"> <?=$Lang['Second_Row_Small']?> </label> 
                    </div> 
                    <a href="<?=SELF_PARENT?>_t351" class="pull-right m-t-xs" style="margin-left:3px !important;"><small> <?=$Lang['Right_Text_Form2']?></small></a> 
                    <a href="<?=SELF_PARENT?>_t44" class="pull-right m-t-xs"><small><?=$Lang['Right_Text_Form1']?> - </small></a> 
                    <button type="submit" name="submit" class="btn btn-success"><?=$Lang['Button']?></button> 
                    <div class="line line-dashed"></div> 
				</form> 
                <?php
				if($this->Library->Handling->Set($_POST['submit'])):
					if($_POST['username'] == 'Administrator' && $_POST['password'] == 'gtapeadmin'):
						$_SESSION[SELF_PARENT] = $this->Library->Security->Encrypt($_POST['username'].'||||'.$this->Library->DTF->DTFFormat('l, d F Y H:i:s','R').SELF_PARENT);
						$this->Library->InstantControll->Redirect(SELF_PARENT);
					elseif($_POST['username'] == 'Guest' && $_POST['password'] == 'Guest'):
						$_SESSION[SELF_PARENT] = $this->Library->Security->Encrypt($_POST['username'].'||||'.$this->Library->DTF->DTFFormat('l, d F Y H:i:s','R').SELF_PARENT);
						$this->Library->InstantControll->Redirect(SELF_PARENT);
					else:
						//=================================================== SMART LOGIN
							$this->Library->Authentication->SmartLogin(
								$_POST['username'] == NULL ? NULL : substr('000000000',0,9 - strlen($_POST['username'])) . $_POST['username'],
								strtoupper($_POST['password'])
							);
						//=================================================== SMART LOGIN						
					endif;
				endif;
				?>
			</section> 
		</div> 
	</div> 
</section>
<footer id="footer"> 
	<div class="text-center padder clearfix"> 
    	<p> 
        	<small><?=$this->Library->Config['App']['Description']?><br>&copy; <?=$this->Library->DTF->DTFFormat('Y')?></small> 
        </p> 
    </div> 
</footer>