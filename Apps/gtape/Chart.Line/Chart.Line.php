<?php
	$Lang	= $this->Library->InstantLanguage->Load();
	$Lang	= $Lang['Table'];
?>
<section class="panel">
	<header class="panel-heading">
    	<?=$Lang['Left_Text_Content']?> <label class="label bg-danger m-l-xs">Line Chart</label>
        <span class="pull-right text-sm"><?=next($Lang)?> : 
        	<span class="dfn">
				<?=$this->Library->InstantInfo->GetLastUpdate(basename(__DIR__),'d F Y - H:i:s');?>
            </span>
        </span>
    </header>
	<div class="panel-body text-center">
        <div id="CHART" class="content-chart">
        	
        </div>
    </div>
</section>