<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		Load.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

require 'Auto.php';
$Load		= new SplClassLoader('Core',__DIR__ . '/');
$Load->Register();

use Core\Bin\Constructor\Constructor;

$Khafii		= new Constructor();