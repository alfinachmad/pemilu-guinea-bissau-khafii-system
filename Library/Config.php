<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		Config.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

$Config = array(
	'Default' 		=> 'gtape',
	'Admin'			=> 'gtapeAdmin',
	'Error'			=> 0,
	'DateTimeZone' 	=> 'Africa/Bissau',
	'Application' => array(
		'gtape' => array(
			'App' => array(
				'Name' 		=> 'GTAPE',
				'Acronym' 	=> 'GABINETE T&Eacute;CNICO DE APOIO AO PROCESSO ELEITORAL',
				'Copyright' => 'Copyright CV. Wahana Cipta 2013',
				'Description'	=> 'Gabinete T&eacute;cnico de Apoio ao Processo Eleitoral - Electoral Information System',
				'MobileVersion' => 'YES',
			),
			'DB' => array(
				'Status' => 'SafeMode', 
				'Type' => 'Oracle',
				'User' => 'dataumum',
				'Pass' => 'dataumum',
				'Host' => '192.168.1.234',
				'Name' => 'GTAPEGB'
			),
			'HTML' => array(
				'Charset' 	=> 'utf-8',
				'Language'	=> 'English'	
			)
		),
		'gtapeAdmin' => array(
			'App' => array(
				'Name' 		=> 'GTAPE - Administrator',
				'Acronym' 	=> 'GABINETE T&Eacute;CNICO DE APOIO AO PROCESSO ELEITORAL - Administrator',
				'Copyright' => 'Copyright CV. Wahana Cipta 2013',
				'Description'	=> 'Gabinete T&eacute;cnico de Apoio ao Processo Eleitoral - Administrator',
				'MobileVersion' => 'YES',
			),
			'DB' => array(
				'Status' => 'Disable',
				'Type' => '',
				'User' => '',
				'Pass' => '',
				'Host' => '',
				'Name' => ''
			),
			'HTML' => array(
				'Charset' 	=> 'utf-8',
				'Language'	=> 'en'
			)
		)		
	)
);
$All		= $Config;
$App		= $Config['Application'];
$Init 		= $Config['Application'][$Config['Default']];
$Default 	= $Config['Default'];