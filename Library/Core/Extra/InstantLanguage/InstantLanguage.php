<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		InstantLanguage.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */
 
namespace Core\Extra\InstantLanguage;

class InstantLanguage
{
	public $Library;
	public function __construct($ImportLibrary) 
	{
		$this->Library = $ImportLibrary;
	}
	public function Load()
	{
		$Lang = parse_ini_file('lang.ini',true);
		if($_SESSION['langToken'] == NULL || !isset($_SESSION)):
			$_SESSION['langToken'] = $this->Library->Config['HTML']['Language'];
			return $Lang[$_SESSION['langToken']];
		else:
			return $Lang[$_SESSION['langToken']];
		endif;
	}
}