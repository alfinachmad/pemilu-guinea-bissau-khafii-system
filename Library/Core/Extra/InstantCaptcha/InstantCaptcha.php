<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		InstantCaptcha.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */
 
namespace Core\Extra\InstantCaptcha;

class InstantCaptcha
{
	public $Library;
	public function __construct($ImportLibrary) 
	{
		$this->Library = $ImportLibrary;
	}
	public function Captcha()
	{
		$Dir	= getcwd() . '\\' . 'Assets\\' . SELF_PARENT . '\\Fonts\\';  
		$String	= '';
		for($i=0;$i<5;$i++):
			$String .= chr(rand(97,122));
		endfor;
		$_SESSION['captcha'.SELF_PARENT] = $String;
		$image = imagecreatetruecolor(170, 60);
		$black = imagecolorallocate($image, 0, 0, 0);
		$color = imagecolorallocate($image, 200, 100, 90); // red
		$white = imagecolorallocate($image, 255, 255, 255);
		imagefilledrectangle($image,0,0,200,100,$white);
		imagettftext($image, 30, 0, 10, 40, $color, $Dir."OpenSans-Light.ttf", $_SESSION['captcha'.SELF_PARENT]);
		#header("Content-type: image/png");
		return imagepng($image);
	}
}