<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		InstantInfo.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

namespace Core\Extra\InstantInfo;

class InstantInfo
{
	public $Library;
	public function __construct($ImportLibrary) 
	{
		$this->Library = $ImportLibrary;
	}
	public function GetLastUpdate($File,$Format)
	{
		$Path = getcwd() . '/' . _APPS_ . '/' . PARENT . '/' . $File . '/' . $File . '.php';
		if (is_file($Path)):
			$filePath = $Path;
			if (!realpath($filePath)):
				$filePath = $_SERVER["DOCUMENT_ROOT"].$filePath;
			endif;
			$fileDate = filemtime($filePath);
			if ($fileDate):
				$fileDate = date("$Format",$fileDate);
				return $fileDate;
			endif;
			return false;
		endif;
		return false;
	}
}