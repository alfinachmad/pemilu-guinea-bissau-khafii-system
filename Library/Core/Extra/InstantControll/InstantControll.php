<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		InstantControll.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

namespace Core\Extra\InstantControll;

class InstantControll 
{
	public $Library;
	public function __construct($ImportLibrary) 
	{
		$this->Library = $ImportLibrary;
	}
	public function Redirect($Path=NULL)
	{
		header('Location: ' . $Path);
	}
	public function Route()
	{
		
	}
	public function GetParent()
	{
		return basename(__DIR__);
	}
}

?>