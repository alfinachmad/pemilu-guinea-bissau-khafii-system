<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		Captcha.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */
 
namespace Core\Bin\Captcha;

class Captcha
{
	public $Library;
	public function __construct($ImportLibrary) 
	{
		$this->Library = $ImportLibrary;
	}
}