<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		String.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

namespace Core\Bin\String;

class String 
{
	public $Library;
	public function __construct($ImportLibrary)
	{
		$this->Library = $ImportLibrary;
	}
	public function Slash($Str=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo addslashes($Str);
		elseif($Output == 'R'):
			return addslashes($Str);
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function BinToHex($Str=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo bin2hex($Str);
		elseif($Output == 'R'):
			return bin2hex($Str);
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function ChrASCII($ASCII=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo chr($ASCII);
		elseif($Output == 'R'):
			return chr($ASCII);
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function Charset($String=NULL,$Input=NULL,$Output=NULL)
	{
		return iconv($Input == NULL ? 'ISO-8859-1' : $Input,'UTF-8',$String);
	}
	public function Exploding($Str=NULL,$Delimiter=NULL)
	{
		return explode($Delimiter, $Str);
	}
	public function HexToBin($Str=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo hex2bin($Str);
		elseif($Output == 'R'):
			return hex2bin($Str);
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function HTMLDecode($Str=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo html_entity_decode($Str);
		elseif($Output == 'R'):
			return html_entity_decode($Str);
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function HTMLEntiti($Str=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo htmlentities($Str);
		elseif($Output == 'R'):
			return htmlentities($Str);
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function HTMLSpecialDecode($Str=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo htmlspecialchars_decode($Str);	
		elseif($Output == 'R'):
			return htmlspecialchars_decode($Str);	
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function HTMLSpecialEncode($Str=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo htmlspecialchars($Str);
		elseif($Output == 'R'):
			return htmlspecialchars($Str);
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function Imploding($Delimiter=NULL,$Str=NULL)
	{
		if(is_array($Str)):
			echo implode($Delimiter, $Str);
		else:
			echo 'Format String yang akan di <i>implode</i> harus dalama bentuk array';
		endif;
	}
	public function Length($Str=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo strlen($Str);
		elseif($Output == 'R'):
			return strlen($Str);
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function Lower($Str=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo strtolower($Str);
		elseif($Output == 'R'):
			return strtolower($Str);
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function LowerFirst($Str=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo lcfirst($Str);
		elseif($Output == 'R'):
			return lcfirst($Str);
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function NumFormat($Str=NULL,$Decimal=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo number_format($Str,$Decimal,',','.');
		elseif($Output == 'R'):
			return number_format($Str,$Decimal,',','.');
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function Pad($Str=NULL,$Length=NULL,$Replace=NULL,$Type=NULL,$Output=NULL)
	{
		if(is_int($Length) && $Replace == NULL && $Type == NULL):
			if($Output == NULL):
				echo str_pad($Str,$Length);
			elseif($Output == 'R'):
				return str_pad($Str,$Length);
			else:
				echo 'Format <i>output</i> yang Anda masukkan salah';
			endif;
		else:
			if($Output == NULL):
				echo str_pad($Str,$Length,$Replace,$Type);
			elseif($Output == 'R'):
				return str_pad($Str,$Length,$Replace,$Type);
			else:
				echo 'Format <i>output</i> yang Anda masukkan salah';
			endif;
		endif;
	}
	public function Repeat($Str=NULL,$Repeat=NULL,$Output=NULL)
	{
		if(is_int($Repeat)):
			if($Output == NULL):
				echo str_repeat($Str,$Repeat);
			elseif($Output == 'R'):
				return str_repeat($Str,$Repeat);
			else:
				echo 'Format <i>output</i> yang Anda masukkan salah';
			endif;
		else:
			echo 'Format <i>Repeat</i> yang Anda masukkan harus berupa type data <i>integer</i>';
		endif;
	}
	public function Replace($Str=NULL,$Replace=NULL,$Subject=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo str_replace($Str,$Replace,$Subject,$Output);
		elseif($Output == 'R'):
			return str_replace($Str,$Replace,$Subject,$Output);
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function StringStr($Haystack=NULL,$Needle=NULL,$Before=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo strstr($Haystack,$Needle,$Before);
		elseif($Output == 'R'):
			return strstr($Haystack,$Needle,$Before);
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function Sub($Str=NULL,$Start=NULL,$Length=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo substr($Str,$Start,$Length);
		elseif($Output == 'R'):
			return substr($Str,$Start,$Length);
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function Upper($Str=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo strtoupper($Str);
		elseif($Output == 'R'):
			return strtoupper($Str);
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function UpperFirst($Str=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo ucfirst($Str);
		elseif($Output == 'R'):
			return ucfirst($Str);
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function UpperWords($Str=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo ucwords($Str);
		elseif($Output == 'R'):
			return ucwords($Str);
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
}

?>