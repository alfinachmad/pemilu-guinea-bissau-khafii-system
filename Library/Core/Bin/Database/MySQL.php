<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		MySQL.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

namespace Core\Bin\Database;

class MySQL 
{
	public $Library;
	public function __construct($ImportLibrary) 
	{
		$this->Library = $ImportLibrary;
	}
	public function Connect($Read=NULL)
	{
		$Connection = mysqli_connect($this->Library['Host'], $this->Library['User'], $this->Library['Pass'], $this->Library['Name']);
		if($Connection):
			if($Read == NULL):
				return $Connection;
			else:
				echo "Connection Success";
			endif;
		else:
			if($Read == NULL):
				return $Connection;
			else:
				echo "Connection Failed";
			endif;
		endif;
	}
	public function FetchArray($Query)
	{
		return mysqli_fetch_array($Query);
	}
	public function FetchAssoc($Query)
	{
		return mysqli_fetch_assoc($Query);
	}
	public function FetchObject($Query)
	{
		return mysqli_fetch_object($Query);
	}
	public function FetchRow($Query)
	{
		return mysqli_fetch_row($Query);
	}
	public function NumRows($Query)
	{
		return mysqli_num_rows($Query);
	}
}

?>