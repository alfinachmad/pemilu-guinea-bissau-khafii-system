<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		Oracle.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

namespace Core\Bin\Database;

class Oracle 
{
	public $Library;
	public function __construct($ImportLibrary)
	{
		$this->Library = $ImportLibrary;
		$this->Connect();
	}
	public function Bind($Parse,$Bind)
	{
		if(is_array($Bind)):
			for($i=0;$i<count($Bind);$i++):
				oci_bind_by_name($Parse, "{$Bind[$i][0]}", $Bind[$i][1]);
			endfor;
		endif;
	}
	public function Connect($Read=NULL)
	{
		if($this->Library['Status'] == 'Disable' || $this->Library['Status'] == 'SafeMode' || $this->Library['Status'] == ''):
			NULL;
		elseif($this->Library['Status'] == 'Enable'):
			$Connection = oci_connect($this->Library['User'], $this->Library['Pass'], $this->Library['Host'] . '/' . $this->Library['Name']);
			if($Connection):
				if($Read == NULL):
					return $Connection;
				else:
					echo "Connection Success";
				endif;
			else:
				if($Read == NULL):
					return $Connection;
				else:
					echo "Connection Failed";
				endif;
			endif;
		endif;
	}
	public function Cursor()
	{
		return oci_new_cursor($this->Connect());
	}
	public function Execute($Execute)
	{
		$Exe = "";
		if(is_array($Execute)):
			for($i=0;$i<count($Execute);$i++):
				$Exe .= oci_execute($Execute[$i]);
			endfor;
		endif;
		return $Exe;
	}
	public function Query($SQLCommand)
	{
		return oci_parse($this->Connect(), $SQLCommand);
	}
	public function SetRecap($Parse,$Cursor)
	{
		oci_bind_by_name($Parse, ":REKAP", $Cursor, -1, OCI_B_CURSOR);
	}
	public function FetchAll($Statement,$Result)
	{
		return oci_fetch_all($Statement,$Result);
	}
	public function FetchArray($Query)
	{
		return oci_fetch_array($Query);
	}
	public function FetchAssoc($Query)
	{
		return oci_fetch_assoc($Query);
	}
	public function FetchObject($Query)
	{
		return oci_fetch_object($Query);
	}
	public function FetchRow($Query)
	{
		return oci_fetch_row($Query);
	}
	public function FreeStatement($Statement)
	{
		return oci_free_statement($Statement);	
	}
	public function NumRows($Query)
	{
		return oci_num_rows($Query);
	}
}
?>