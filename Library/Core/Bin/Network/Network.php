<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		Network.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

namespace Core\Bin\Network;

class Network 
{
	public $Library;
	public function __construct($ImportLibrary) 
	{
		$this->Library = $ImportLibrary;
	}
	public function Header($Command=NULL)
	{
		if($Command == NULL):
			echo 'Parameter tidak boleh kosong';
		else:
			header($Command);
		endif;
	}
	public function HeaderRefresh($Time=NULL,$Path=NULL)
	{
		$Redirect = '';
		if($Path == 'SELF'):
			$Redirect = SELF_PARENT.SELF_SEPARATE.SELF_SUB_PARENT;
		else:
			$Redirect = $Path;
		endif;
		if($Path == NULL && $Time == NULL):
			header('refresh:180;url=' . SELF_PARENT.SELF_SEPARATE.SELF_SUB_PARENT);
		else:
			header('refresh:' . $Time . ';url=' . $Redirect);
		endif;
	}
}