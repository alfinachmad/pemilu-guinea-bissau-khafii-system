<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		Session.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

namespace Core\Bin\Session;

class Session 
{
	public $Library;
	public function __construct($ImportLibrary) {
		$this->Library = $ImportLibrary;
		$this->SessionStart();
	}
	public function SessionDestroy()
	{
		session_destroy();
	}
	public function SessionName($Name=NULL)
	{
		return session_name($Name);
	}
	public function SessionModule($Module=NULL)
	{
		return session_module_name($Module);
	}
	public function SessionSavePath($Path=NULL)
	{
		return session_save_path($Path);
	}
	public function SessionId($Id=NULL)
	{
		return session_id($Id);
	}
	public function SessionRegenerate($Old=NULL)
	{
		return session_regenerate_id($Old);
	}
	public function SessionEncode($Data=NULL)
	{
		return session_encode();
	}
	public function SessionDecode($Data=NULL)
	{
		return session_decode($Data);
	}
	public function SessionStart()
	{
		#session_start();
	}
	public function SessionUnset($Session=NULL)
	{
		unset($Session);
	}
	public function SetSession($Name,$Value,$Read=NULL)
	{
		if($Read == NULL):
			$_SESSION[$Name] = $Value;
		else:
			echo $_SESSION[$Name] = $Value;
		endif;
	}
}
?>