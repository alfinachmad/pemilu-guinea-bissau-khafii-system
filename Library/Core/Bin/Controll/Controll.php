<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		Controll.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

namespace Core\Bin\Controll;

class Controll 
{
	public $Library;
	public function __construct($ImportLibrary) 
	{
		$this->Library 	= $ImportLibrary;
	}
	public function Including($Path=NULL,$Format=NULL)
	{
		if($Path == NULL):
			$this->Library->Error->ERROR_FILESYSTEM;
		else:
			if($Format == NULL):
				include $Path;
			elseif($Format == 'ONCE'):
				include_once $Path;
			else:
				$this->Library->Error->ERROR_FILESYSTEM;
			endif;
		endif;
	}
	public function Link($Type=NULL)
	{
		if($Type == 'PARENT'):
			echo SELF_PARENT;
		elseif($Type == 'SUB'):
			echo SELF_SEPARATE;
		elseif($Type == 'SUB-PARENT'):
			echo SELF_SUB_PARENT;
		elseif($Type == 'LINK-PARENT'):
			return SELF_PARENT . SELF_SEPARATE;
		else:
			
		endif;
	}
	public function Redirect($Location)
	{
		header('Location: ' . $Location);
	}
	public function Requiring($Path=NULL,$Format=NULL)
	{
		if($Path == NULL):
			$this->Library->Error->ERROR_FILESYSTEM;
		else:
			if($Format == NULL):
				require $Path;
			elseif($Format == 'ONCE'):
				require_once $Path;
			else:
				$this->Library->Error->ERROR_FILESYSTEM;
			endif;
		endif;
	}
	public function Routing($Folder=NULL,$Sub=NULL)
	{
		$Inc	= NULL;
		if($Folder == NULL):
			$Inc = include _APPS_ . '/' . SELF_PARENT . '/' . 'Index.php';
			(!$Inc ? header('Location: '.SELF_PARENT.'!error.index') : $Inc);
		else:
			$Inc = include _APPS_ . '/' . SELF_PARENT . '/' . $Folder . '/' . ($Sub != NULL ? $Sub : $Folder) . '.php';
			(!$Inc ? header('Location: '.SELF_PARENT.'!error.index') : $Inc);
		endif;
	}
}

?>