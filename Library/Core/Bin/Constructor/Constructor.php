<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		Constructor.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

namespace Core\Bin\Constructor;

use Core\Bin\ArrayOfObject\ArrayOfObject as ArrayObject,
	Core\Bin\Controll\Controll as Controll,
	Core\Bin\Database as DB,
	Core\Bin\DTF\DTF as DateTime,
	Core\Bin\Error\Error as Error,
	Core\Bin\FileSystem\FileSystem as FileSystem,
	Core\Bin\Handling\Handling as Handling,
	Core\Bin\HTML\InstantModel as InstantHTML,
	Core\Bin\HTML\Tag as TagHTML,
	Core\Bin\Magic\Magic as _MAGIC_,
	Core\Bin\Network\Network as Networking,
	Core\Bin\Output\Output as Buffer,
	Core\Bin\Security\Security as Security,
	Core\Bin\Session\Session as Sessions,
	Core\Bin\String\String as String,
	Core\Controll\Elector\Elector as Elector,
	Core\Controll\Region\Region as Region,
	Core\Extra\InstantCaptcha\InstantCaptcha as InstantCaptcha,
	Core\Extra\InstantControll\InstantControll as InstantControll,
	Core\Extra\InstantInfo\InstantInfo as InstantInfo,
	Core\Extra\InstantLanguage\InstantLanguage as InstantLanguage;

class Constructor 
{
	public 
	$All,$Array,$App,$Captcha,$Class,$Config,$Controll,$Database,$Default,$DTF,$Elector,$Error,$Extra,$FileSystem,$Handling,
	$HTML,$InstantCaptcha,$InstantControll,$InstantInfo,$InstantLanguage,$Magic,$Network,$Pointer,$Output,$Region,$Security,$Session,$String,$Tag,$Library;
	public function __construct() 
	{
		$All		= NULL;
		$Init 		= NULL;
		$Default 	= NULL;
		
		define("ROOT", getcwd() . '//');
		define("BASEPATH", ROOT . "Library");
		define('PARENT' , $_GET['id']);
		define("Stylsheet", PARENT . '/s/');
		define("Image", PARENT . '/i/');
		define("Javascript", PARENT . '/j/');
		define("Font", PARENT . '/f/');
		define("SELF_PARENT", @$_GET['id']);
		define("SELF_SUB_PARENT", @$_GET['mode']);
		define("SECURE_URL", @$_GET['session']);
		define("SELF_SEPARATE", "!");
		define("REALURL",SELF_PARENT.SELF_SEPARATE.SELF_SUB_PARENT);
		define("ERROR404",PARENT.SELF_SEPARATE.'error.index');
		include_once BASEPATH . '/' . 'Config.php';
		
		date_default_timezone_set($All['DateTimeZone']);
		error_reporting($All['Error']);
		
		$this->All				= $All;
		$this->App				= $App;
		#$this->Array			= $this->ArrayOfObject();
		$this->Config 			= $Init;
		$this->Captcha			= $this->InstantCaptcha();
		$this->Controll			= $this->Controll();
		$this->Database 		= $this->Database();
		$this->Default			= $Default;
		$this->DTF				= $this->DTF();
		$this->Error			= $this->Error();
		$this->Elector			= $this->Elector();
		$this->FileSystem		= $this->FileSystem();
		$this->Handling			= $this->Handling();
		#$this->HTML			= $this->HTML();
		$this->InstantControll	= $this->InstantControll();
		$this->InstantInfo		= $this->InstantInfo();
		$this->InstantLanguage	= $this->InstantLanguage();
		#$this->Magic			= $this->Magic();
		$this->Network			= $this->Network();
		$this->Output			= $this->Output();
		$this->Region			= $this->Region();
		$this->Security			= $this->Security();
		$this->Session			= $this->Session();
		$this->String			= $this->String();
		$this->Tag				= $this->Tag();
	}
	private function ArrayOfObject()
	{
		return new ArrayObject($this);
	}
	private function Chart()
	{
		return new Chart($this);
	}
	private function Controll()
	{
		return new Controll($this);
	}
	private function Database()
	{
		return new DB\Oracle($this->App[SELF_PARENT]['DB']);
	}
	private function DTF()
	{
		return new DateTime($this);
	}
	private function Elector()
	{
		return new Elector($this);
	}
	private function Error()
	{
		return new Error($this);
	}
	private function FileSystem()
	{
		return new FileSystem($this);
	}
	private function Handling()
	{
		return new Handling($this);
	}
	private function HTML()
	{
		return new InstantHTML($this);
	}
	private function InstantControll()
	{
		return new InstantControll($this);
	}
	private function InstantCaptcha()
	{
		return new InstantCaptcha($this);
	}
	private function InstantInfo()
	{
		return new InstantInfo($this);
	}
	private function InstantLanguage()
	{
		return new InstantLanguage($this);
	}
	private function Magic()
	{
		return new _MAGIC_($this);
	}
	private function Network()
	{
		return new Networking($this);
	}
	private function Output()
	{
		return new Buffer($this);
	}
	private function Region()
	{
		return new Region($this);
	}
	private function Security()
	{
		return new Security($this);
	}
	private function Session()
	{
		return new Sessions($this);
	}
	private function String()
	{
		return new String($this);
	}
	private function Tag()
	{
		return new TagHTML($this);
	}
}
?>