<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		DTF.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

namespace Core\Bin\DTF;

class DTF 
{
	public $Library;
	public function __construct($ImportLibrary) 
	{
		$this->Library = $ImportLibrary;
	}
	public function DTFFormat($Format=NULL,$Output=NULL)
	{
		if($Format == NULL):
			if($Output == NULL):
				echo date('l, d F Y H:i:s');
			else:
				return date('l, d F Y H:i:s');
			endif;
		else:
			if($Output == NULL):
				echo date($Format);
			else:
				return date($Format);
			endif;
		endif;
	}
	public function DTFGet($Array=NULL,$Output=NULL)
	{
		$Get = getdate();
		if($Array == NULL):
			echo $this->Library->Error->ERROR_FILESYSTEM();
		else:
			if($Output == NULL):
				echo $Get[$Array];
			else:
				return $Get[$Array];
			endif;
		endif;
	}
}

?>