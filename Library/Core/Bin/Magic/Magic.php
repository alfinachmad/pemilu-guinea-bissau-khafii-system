<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		InstantModel.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

namespace Core\Bin\Magic;

class Magic 
{
	public $Library;
	public function __construct($ImportLibrary) 
	{
		$this->Library = $ImportLibrary;
	}
	public function DIR()
	{
		return __DIR__;
	}
	public function E($String=NULL)
	{
		echo $String;
	}
	public function FILE()
	{
		return __FILE__;
	}
	public function SERVER($Format=NULL)
	{
		return $_SERVER[$Format];
	}
}

?>