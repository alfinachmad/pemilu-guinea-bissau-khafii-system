<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		FileSystem.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

namespace Core\Bin\FileSystem;

class FileSystem 
{
	public $Library;
	public function __construct($ImportLibrary)
	{
		$this->Library = $ImportLibrary;
	}
	public function BasePath($Str=NULL,$Output=NULL)
	{
		if($Output == NULL):
			echo basename($Str);
		elseif($Output == 'R'):
			return basename($Str);
		else:
			echo 'Format <i>output</i> yang Anda masukkan salah';
		endif;
	}
	public function ChangeGroup($File=NULL,$Type=NULL)
	{
		chgrp($File,$Type);
	}
	public function ChangeMode($File=NULL,$Mode=NULL)
	{
		if(is_int($Mode)):
			chmod($File,$Mode);
		else:
			echo 'Format <i>Mode</i> yang Anda masukkan harus berupa type data <i>integer</i>';
		endif;
	}
	public function ChangeOwner($File=NULL,$Own=NULL)
	{
		if($File == NULL):
		
		else:
			chown($File,$Own);
		endif;
	}
	public function CheckDir($Path=NULL)
	{
		is_dir($Path);
	}
	public function CheckExec($Path=NULL)
	{
		is_executable($Path);
	}
	public function CheckFile($Path=NULL)
	{
		is_file($Path);
	}
	public function CheckLink($Path=NULL)
	{
		is_link($Path);
	}
	public function CheckUpload($Path=NULL)
	{
		is_uploaded_file($Path);
	}
	public function CheckWrite($Path=NULL)
	{
		is_writeable($Path);
	}
	public function ClearState()
	{
		clearstatcache();
	}
	public function CopyFile($FileSource,$FileTarget)
	{
		if($FileSource == NULL && $FileTarget == NULL):
		
		else:
			copy($FileSource,$FileTarget);
		endif;
	}
	public function GetDirName($Path=NULL)
	{
		if($Path == NULL):
			$this->Library->Error->ERROR_FILESYSTEM();
		else:
			echo dirname($Path);
		endif;
	}
	public function GetDiskFree($Dir=NULL)
	{
		if($Dir == NULL):
			$this->Library->Error->ERROR_FILESYSTEM();
		else:
			echo disk_free_space($Dir);
		endif;
	}
	public function GetDiskTotal($Dir=NULL)
	{
		if($Dir == NULL):
			$this->Library->Error->ERROR_FILESYSTEM();
		else:
			echo disk_total_space($Dir);
		endif;
	}
	public function Exists($Path=NULL)
	{
		if($Path == NULL):
			$this->Library->Error->ERROR_FILESYSTEM();
		else:
			return file_exists($Path);
		endif;
	}
	public function FileAccessTime($Path=NULL)
	{
		if($Path == NULL):
			$this->Library->Error->ERROR_PATH();
		else:
			return fileatime($Path);
		endif;
	}
	public function FileChangeTime($Path=NULL)
	{
		if($Path == NULL):
			$this->Library->Error->ERROR_PATH();
		else:
			return filectime($Path);
		endif;
	}
	public function FileGet($File=NULL,$Data=NULL)
	{
		return file_get_contents($File,$Data);
	}
	public function FileGroup($Path=NULL)
	{
		if($Path == NULL):
			$this->Library->Error->ERROR_PATH();
		else:
			return filegroup($Path);
		endif;
	}
	public function FileModifiedTime($Path=NULL)
	{
		if($Path == NULL):
			$this->Library->Error->ERROR_PATH();
		else:
			return filemtime($Path);
		endif;
	}
	public function FileOwner($Path=NULL)
	{
		if($Path == NULL):
			$this->Library->Error->ERROR_PATH();
		else:
			return fileowner($Path);
		endif;
	}
	public function FilePermission($Path=NULL)
	{
		if($Path == NULL):
			$this->Library->Error->ERROR_PATH();
		else:
			return fileperms($Path);
		endif;
	}
	public function FilePut($File=NULL,$Data=NULL)
	{
		return file_put_contents($File,$Data);
	}
	public function FileSize($Path=NULL)
	{
		if($Path == NULL):
			$this->Library->Error->ERROR_PATH();
		else:
			return filesize($Path);
		endif;
	}
	public function FileType($Path=NULL)
	{
		if($Path == NULL):
			$this->Library->Error->ERROR_PATH();
		else:
			return filetype($Path);
		endif;
	}
	public function MakeDirectory($Path=NULL,$Mode=NULL)
	{
		mkdir($Path,$Mode);
	}
	public function MoveFile($File=NULL,$Path=NULL)
	{
		move_uploaded_file($File,$Path);
	}
	public function ParsingFile($Path=NULL,$Process=NULL)
	{
		parse_ini_file($Path,$Process);
	}
	public function Path($Path=NULL,$Options=NULL)
	{
		pathinfo($Path,$Options);
	}
	public function RealPathFile($Path=NULL)
	{
		realpath($Path);
	}
	public function RenameDirectory($Oldname=NULL,$Newname=NULL)
	{
		rename($Oldname,$Newname);
	}
	public function RemoveDirectory($Path=NULL)
	{
		rmdir($Path);
	}
	public function RemoveFile($Path=NULL)
	{
		unlink($Path);
	}
}

?>