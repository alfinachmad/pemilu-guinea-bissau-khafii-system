<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		InstantModel.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

namespace Core\Bin\HTML;

class InstantModel 
{
	public $Library,$ConfigHTML;
	public function __construct($ImportLibrary) 
	{
		$this->Library		= $ImportLibrary;
		$this->ConfigHTML	= $this->Library->Config['Application'];
		$this->Starter();
		$this->StyleSheet(
			array(
				'basic',
				'fonts'
			)
		);
		echo '</head><body>';
	}
	private function Starter()
	{
		$Starter  = '';
		$Starter .= '<!DOCTYPE html>';
		$Starter .= '<html lang="' . $this->ConfigHTML['HTML']['Language'] . '">';
		$Starter .= '<head>';
		$Starter .= '<title>' . $this->ConfigHTML['App']['Description'] . '</title>';
		$Starter .= '<meta charset="' . $this->ConfigHTML['HTML']['Charset'] . '">';
		$Starter .= '<meta name="description" content="' . $this->ConfigHTML['App']['Description'] . '">';
		$Starter .= '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">';
		echo $Starter;
	}
	public function StyleSheet($Input)
	{
		$Style	= '';
		if(is_array($Input)):
			for($i=0;$i<count($Input);$i++):
				$Style .= '<link rel="stylesheet" href="' . 'gtape/' . 's/' . $Input[$i] . '">';
			endfor;
			echo $Style;
		endif;
	}
	public function Script($Input)
	{
		$Script	= '';
		if(is_array($Input)):
			for($i=0;$i<count($Input);$i++):
				$Script .= '<link rel="stylesheet" href="' . 'gtape/' . 's/' . $Input[$i] . '">';
			endfor;
			echo $Script;
		endif;
	}
}

?>