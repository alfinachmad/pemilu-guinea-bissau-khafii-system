<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		Tag.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

namespace Core\Bin\HTML;

class Tag 
{
	public $Library;
	public function __construct($ImportLibrary) 
	{
		$this->Library = $ImportLibrary;
	}
	public function Doctype()
	{
		echo '<!DOCTYPE html>';
	}
	public function HLang($Lang=NULL)
	{
		if($Lang == NULL):
			echo '<html lang="en">';
		else:
			echo '<html lang="$Lang">';
		endif;
	}
	public function Head()
	{
		echo '<head>';
	}
	public function Meta($Charset=NULL)
	{
		if($Charset == NULL):
			echo '<meta charset="utf-8">';
		else:
			echo '<meta charset="$Charset">';
		endif;
	}
	public function Title()
	{
		echo '<title></title>';
	}
	public function EndHead()
	{
		echo '</head>';
	}
	public function EndHTML()
	{
		echo '</html>';
	}
}

?>