<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		Output.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

namespace Core\Bin\Output;

class Output 
{
	public $Library;
	public function __construct($ImportLibrary) 
	{
		$this->Library = $ImportLibrary;
	}
	public function Clean()
	{
		ob_clean();
	}
	public function EndClean()
	{
		ob_end_clean();
	}
	public function EndFlush()
	{
		ob_end_flush();
	}
	public function Flush()
	{
		ob_flush();
	}
	public function GetClean()
	{
		ob_get_clean();
	}
	public function GetContents()
	{
		ob_get_contents();
	}
	public function GetFlush()
	{
		ob_get_flush();
	}
	public function GetLength()
	{
		ob_get_length();
	}
	public function GetLevel()
	{
		ob_get_level();
	}
	public function GetStat($Status=NULL)
	{
		if($Status == NULL):
			ob_get_status(FALSE);
		else:
			ob_get_status(TRUE);
		endif;
	}
	public function ListHandlers()
	{
		return ob_list_handlers();
	}
	public function Start($Callback=NULL)
	{
		ob_start($Callback);
	}
}

?>