<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		Security.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */

namespace Core\Bin\Security;

class Security
{
	private $Key = '-_-donthackit-_-';
	public $Library;
	public function __construct($ImportLibrary) {
		$this->Library = $ImportLibrary;
	}
	 public function Base64Enc($Data)
	 {
		 return base64_encode($Data);
	 }
	 public function BaseEncrypt64($String)
	 {
		 $v 	= $this->Base64Enc($String);
		 $Data	= str_replace(array('+','/','='),array('-','_',''),$v);
		 return $Data;
	 }
	 public function Base64Dec($Data)
	 {
		  return base64_decode($Data);
	 }
	 public function BaseDecrypt64($String)
	 {
		 $Data = str_replace(array('-','_'),array('+','/'),$String); 
		 $Mode = strlen($Data) % 4; 
		 if($Mode):
		 	$Data .= substr('====', $Mode);
		 endif;
		 return $this->Base64Dec($Data);
	 }
	 public function Encrypt($String)
	 {
		 $IV_Size	= mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		 $IV		= mcrypt_create_iv($IV_Size, MCRYPT_RAND); 
		 $Crypter	= mcrypt_encrypt(MCRYPT_RIJNDAEL_256,$this->Key,$String,MCRYPT_MODE_ECB,$IV);
		 return !$String ? FALSE : trim($this->BaseEncrypt64($Crypter));
	 }
	 public function Decrypt($String)
	 {
		 $Crypter	= $this->BaseDecrypt64($String);  
		 $IV_Size	= mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		 $IV		= mcrypt_create_iv($IV_Size, MCRYPT_RAND); 
		 $Decrypter	= mcrypt_decrypt(MCRYPT_RIJNDAEL_256,$this->Key,$Crypter,MCRYPT_MODE_ECB,$IV); 
		 return !$String ? FALSE : trim($Decrypter);
	 }
	 public function JSONEnc($Data=NULL,$Prefix=NULL)
	 {
		 return json_encode($Data,$Prefix);
	 }
	 public function JSONDec($Data=NULL,$Prefix=NULL)
	 {
		 return json_decode($Data,$Prefix);
	 }
}