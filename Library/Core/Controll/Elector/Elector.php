<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		Elector.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */
 
namespace Core\Controll\Elector;

class Elector
{
	public $Library;
	public function __construct($ImportLibrary) 
	{
		$this->Library = $ImportLibrary;
	}
	public function ByRegion($Region=NULL,$Tgl=NULL,$Loop=NULL)
	{
		
		$Data	= array(
			0,0,2,$Region == NULL ? 'ALL' : $Region,'ALL','ALL','ALL','ALL',$Tgl == NULL ? '17/Mar/14' : date('d/M/y',strtotime($Tgl)),2,0
		);
		$Parse	= $this->Library->Database->Query('
			BEGIN sidelector.package_elektor.elektor_wilayah_rekap
			(
				:GROUP,:INDEX,:BAHASA,:KODE_REGION,:KODE_CIRCULO, 
				:KODE_SEKTOR,:KODE_DE,:KODE_ALDEIA,:TGL1,:PRM1,:AGE1,:REKAP
			); 
			END;
		');
		$this->Library->Database->Bind($Parse,
			array(
				0 => array(':GROUP',$Data[0]),
				1 => array(':INDEX',$Data[1]),
				2 => array(':BAHASA',$Data[2]),
				3 => array(':KODE_REGION',$Data[3]),
				4 => array(':KODE_CIRCULO',$Data[4]),
				5 => array(':KODE_SEKTOR',$Data[5]),
				6 => array(':KODE_DE',$Data[6]),
				7 => array(':KODE_ALDEIA',$Data[7]),
				8 => array(':TGL1',$Data[8]),
				9 => array(':PRM1',$Data[9]),
				10 => array(':AGE1',$Data[10])
			)
		);
		$Cursor	= $this->Library->Database->Cursor();
		$this->Library->Database->SetRecap($Parse,$Cursor);
		$this->Library->Database->Execute(
			array($Parse,$Cursor)
		);
		if($Loop == NULL):
			return $Cursor;
		elseif($Loop == 'PARSE'):
			return $Parse;
		else:
			return $this->Library->Database->FetchAssoc($Cursor);
		endif;
	}
	public function ByAge($Region=NULL,$Tgl=NULL,$Loop=NULL)
	{
		$Data	= array(
			0,0,2,$Region == NULL ? 'ALL' : $Region,'ALL','ALL','ALL','ALL',$Tgl == NULL ? '17/Mar/14' : date('d/M/y',strtotime($Tgl)),0,0
		);
		$Parse	= $this->Library->Database->Query('
			BEGIN sidelector.package_elektor.total_elektor_umur_rekap
			(
				:INDEX,:GROUP,:IDENTIC,:NAME,:KODE_REGION,:KODE_CIRCULO,:KODE_SEKTOR,:KODE_DE,:KODE_ALDEIA,:TGL,:AGE1,:AGE2,:REKAP
			); 
			END;
		');
		$this->Library->Database->Bind($Parse,
			array(
				0 => array(':INDEX',$Data[0]),
				1 => array(':GROUP',$Data[1]),
				2 => array(':IDENTIC',$Data[1]),
				3 => array(':NAME',$Data[4]),
				4 => array(':KODE_REGION',$Data[3]),
				5 => array(':KODE_CIRCULO',$Data[4]),
				6 => array(':KODE_SEKTOR',$Data[5]),
				7 => array(':KODE_DE',$Data[6]),
				8 => array(':KODE_ALDEIA',$Data[7]),
				9 => array(':TGL',$Data[8]),
				10 => array(':AGE1',$Data[9]),
				11 => array(':AGE2',$Data[10])
			)
		);
		$Cursor	= $this->Library->Database->Cursor();
		$this->Library->Database->SetRecap($Parse,$Cursor);
		$this->Library->Database->Execute(
			array($Parse,$Cursor)
		);
		if($Loop == NULL):
			return $Cursor;
		elseif($Loop == 'PARSE'):
			return $Parse;
		else:
			return $this->Library->Database->FetchAssoc($Cursor);
		endif;
	}
	public function ByEstimate($Periode=NULL,$Date=NULL)
	{
		$Periode	= '2014/03';
		$Date		= '18/Mar/14';
		$Parse	= $this->Library->Database->Query('
			BEGIN sidelector.package_elektor.estimasi_pendaftaran
			(
				:PERIODE,:TANGGAL,:REKAP
			); 
			END;
		');
		$this->Library->Database->Bind($Parse,
			array(
				0 => array(':PERIODE',$Periode),
				1 => array(':TANGGAL',$Date)
			)
		);
		$Cursor	= $this->Library->Database->Cursor();
		$this->Library->Database->SetRecap($Parse,$Cursor);
		$this->Library->Database->Execute(
			array($Parse,$Cursor)
		);
		return $Cursor;
	}
	public function ByLookup($Kode,$Name,$DOB,$NameOfFather)
	{
		$SQL		= NULL;
		$Null		= '000000';
		$Birth		= $DOB == NULL ? '%' : date('d-M-y',strtotime($DOB));
		$Default	= substr($Null,-6,6 - strlen($Kode)) . $Kode;
		if($Kode <> NULL):
			$SQL	.= 'AND KODE_ELEKTOR LIKE \'%'.$Kode.'%\'';		
		endif;
		if($Name <> NULL):
			$SQL	.= 'AND NAMA LIKE \'%'.strtoupper($Name).'%\'';
		endif;
		if($DOB <> NULL):
			$SQL	.= 'AND TANGGAL_LAHIR LIKE \'%'.strtoupper($DOB).'%\'';
		endif;
		if($NameOfFather <> NULL):
			$SQL	.= 'AND NAMA_AYAH LIKE \'%'.strtoupper($NameOfFather).'%\'';
		endif;
		$Parse	= $this->Library->Database->Query('
			SELECT KODE_ELEKTOR,NAMA,KELAMIN_1,TANGGAL_LAHIR,NAMA_AYAH FROM sidelector.velektor WHERE ROWNUM <= 15 AND KODE_ELEKTOR IS NOT NULL '.$SQL.' ORDER BY KODE_ELEKTOR ASC
		');
		$this->Library->Database->Execute(
			array($Parse)
		);
		return $Parse;
	}
	public function ByNumber($Number=NULL)
	{
		$Language	= 2;
		$Parse	= $this->Library->Database->Query('
			BEGIN sidelector.package_elektor.cetak_kartu 
			( 
				:KODE_ELEKTOR, 
				:BAHASA,
				:REKAP
			);
			END; 
		');
		$this->Library->Database->Bind($Parse,
			array(
				0 => array(':KODE_ELEKTOR',$Number),
				1 => array(':BAHASA',$Language)
			)
		);
		$Cursor	= $this->Library->Database->Cursor();
		$this->Library->Database->SetRecap($Parse,$Cursor);
		$this->Library->Database->Execute(
			array($Parse,$Cursor)
		);
		return $Cursor;
	}
}