<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		Authentication.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */
 
namespace Core\Controll\Authentication;

class Authentication
{
	public $Library;
	public function __construct($ImportLibrary) 
	{
		$this->Library = $ImportLibrary;
	}
	public function SmartLogin($Username=NULL,$Password=NULL)
	{
		$Parse	= $this->Library->Elector->BySecureLogin($Username,$Password);
		$Result	= $this->Library->Database->FetchAssoc($Parse);
		if($Result['KODE_ELEKTOR'] != NULL && $Result['NO_SEGEL'] != NULL):
			$_SESSION['modeprofile']	= 'Active';
			$_SESSION[SELF_PARENT]		= $this->Library->Security->Encrypt(SELF_PARENT.'||||'.$Result['KODE_ELEKTOR'].'||||'.$Result['NO_SEGEL']);
			$this->Library->InstantControll->Redirect(SELF_PARENT);
		else:
			$this->Library->InstantControll->Redirect(SELF_PARENT);
		endif;
	}
}