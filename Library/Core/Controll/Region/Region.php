<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		Region.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */
 
namespace Core\Controll\Region;

class Region
{
	public $Library;
	public function __construct($ImportLibrary) 
	{
		$this->Library = $ImportLibrary;
	}
	public function ListRegion()
	{
		$Parse	= $this->Library->Database->Query('SELECT * FROM sidelector.region ORDER BY KODE_REGION');
		$this->Library->Database->Execute(
			array($Parse)
		);
		return $Parse;
	}
}