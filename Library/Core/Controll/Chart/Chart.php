<?php 
/**
 * Khafii Semi-Framework
 * 
 * @name		Chart.php
 * @author		Alfin Barriky Nur Ilham Achmad (zebrenitza@gmail.com)
 * @version		1.10.1-beta2.013
 * @copyright	CV. Wahana Cipta 2013
 * @since		Oktober 2013
 * @todo		SplClassLoader,Bootstrap,Jquery
 * @license		GPL License v2.0
 */
 
namespace Core\Controll\Chart;

class Chart
{
	public $Library;
	public function __construct($ImportLibrary) 
	{
		$this->Library = $ImportLibrary;
	}
	public function ChartTable($Region=NULL,$Loop=NULL,$Datas=NULL)
	{
		$Data	= array(
			0,0,2,$Region == NULL ? 'ALL' : $Region,'ALL','ALL','ALL','ALL','17/Mar/14',2,18
		);
		$Parse	= $this->Library->Database->Query('
			BEGIN sidelector.package_elektor.elektor_wilayah_rekap
			(
				:GROUP,:INDEX,:BAHASA,:KODE_REGION,:KODE_CIRCULO, 
				:KODE_SEKTOR,:KODE_DE,:KODE_ALDEIA,:TGL1,:PRM1,:AGE1,:REKAP
			); 
			END;
		');
		$this->Library->Database->Bind($Parse,
			array(
				0 => array(':GROUP',$Data[0]),
				1 => array(':INDEX',$Data[1]),
				2 => array(':BAHASA',$Data[2]),
				3 => array(':KODE_REGION',$Data[3]),
				4 => array(':KODE_CIRCULO',$Data[4]),
				5 => array(':KODE_SEKTOR',$Data[5]),
				6 => array(':KODE_DE',$Data[6]),
				7 => array(':KODE_ALDEIA',$Data[7]),
				8 => array(':TGL1',$Data[8]),
				9 => array(':PRM1',$Data[9]),
				10 => array(':AGE1',$Data[10])
			)
		);
		$Cursor	= $this->Library->Database->Cursor();
		$this->Library->Database->SetRecap($Parse,$Cursor);
		$this->Library->Database->Execute(
			array($Parse,$Cursor)
		);
		if($Loop == NULL):
			return $Cursor;
		elseif($Loop == 'PARSE'):
			return $Parse;
		else:
			return $this->Library->Database->FetchAssoc($Cursor);
		endif;
	}
}